﻿using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PubMedLite.Models
{
    public class MainPageLoader : Loader
    {
        private Visibility _searchResultVisibility;

        public Visibility SearchResultVisibility
        {
            get { return _searchResultVisibility; }
            set { this.SetProperty(ref this._searchResultVisibility, value); }
        }

        protected override void SetLoading(bool value)
        {
            if (value == true)
            {
                SearchResultVisibility = Visibility.Collapsed;
            }
            else
            {
                SearchResultVisibility = Visibility.Visible;
            }
            base.SetLoading(value);
        }

    }
}
