﻿using Microsoft.Phone.Info;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    class AppInfoService
    {
        public static long GetDatabaseSize()
        {
            long l = 0;
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isf.FileExists("AppDb.sdf"))
                    {
                        using (IsolatedStorageFileStream dataStream = isf.OpenFile("AppDb.sdf", FileMode.Open))
                        {
                            l = dataStream.Length;
                        }
                    }
                }
            }
            catch
            {
                // handel
            }
            return l;
        }
        public static Double MemoryPercentage { get { double d = (double)DeviceStatus.ApplicationPeakMemoryUsage / (double)DeviceStatus.ApplicationMemoryUsageLimit * 100; return d; } }
    }
}
