﻿using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    [DataContract]
    public class MainPageTombstoneState
    {
        [DataMember]
        public int SelectedPageIndex { get; set; }
        [DataMember]
        public int OpenSearchId { get; set; }
    }
}
