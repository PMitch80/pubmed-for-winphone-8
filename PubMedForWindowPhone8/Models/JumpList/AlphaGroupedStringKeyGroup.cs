﻿using Microsoft.Phone.Globalization;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class AlphaGroupedStringKeyGroup<T> : NestedKeyGroupBase<StringKeyGroup<T>, T>
    {

        private StringKeyGroup<T> _subKey;

        private AlphaGroupedStringKeyGroup(string key)
        {
            _key = key;
        }

        private AlphaGroupedStringKeyGroup(string key, IEnumerable<StringKeyGroup<T>> list)
            : base(list)
        {
            _key = key;
        }

        public AlphaGroupedStringKeyGroup(StringKeyGroup<T> subKey)
        {
            _subKey = subKey;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="slg">The </param>
        /// <returns>Theitems source for a LongListSelector</returns>
        private static List<AlphaGroupedStringKeyGroup<T>> CreateGroups(SortedLocaleGrouping slg)
        {
            List<AlphaGroupedStringKeyGroup<T>> list = new List<AlphaGroupedStringKeyGroup<T>>();

            foreach (string key in slg.GroupDisplayNames)
            {
                AlphaGroupedStringKeyGroup<T> keygroup = new AlphaGroupedStringKeyGroup<T>(key);
                keygroup.BaseList = new List<StringKeyGroup<T>>();
                list.Add(keygroup);
            }

            return list;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="items">The items to place in the groups.</param>
        /// <param name="ci">The CultureInfo to group and sort by.</param>
        /// <param name="getKey">A delegate to get the key from an item.</param>
        /// <param name="sort">Will sort the data if true.</param>
        /// <returns>An items source for a LongListSelector</returns>
        public List<AlphaGroupedStringKeyGroup<T>> BuildList(IEnumerable<StringKeyGroup<T>> items, bool sort)
        {
            CultureInfo ci = CultureInfo.CurrentCulture;
            SortedLocaleGrouping slg = new SortedLocaleGrouping(ci);
            List<AlphaGroupedStringKeyGroup<T>> list = CreateGroups(slg);

            foreach (StringKeyGroup<T> item in items)
            {
                int index = 0;
                
                try
                {
                    index = slg.GetGroupIndex(item.Key);
                }
                catch(NullReferenceException)
                {
                }
                
                if (index >= 0 && index < list.Count)
                {
                    list[index].BaseList.Add(item);
                }
            }

            if (sort)
            {
                foreach (AlphaGroupedStringKeyGroup<T> group in list)
                {
                    group.BaseList.Sort((c0, c1) => { return ci.CompareInfo.Compare(c0.Key, c1.Key); });
                }
            }

            return list;
        }

        public override ObservableCollection<K> BuildCollection<K>(IEnumerable<StringKeyGroup<T>> source)
        {
            if (typeof(K) == typeof(AlphaGroupedStringKeyGroup<T>))
            {
                List<AlphaGroupedStringKeyGroup<T>> list = BuildList(source, true);
                List<AlphaGroupedStringKeyGroup<T>> listOfCollections = new List<AlphaGroupedStringKeyGroup<T>>();
                foreach (AlphaGroupedStringKeyGroup<T> item in list)
                {
                    listOfCollections.Add(new AlphaGroupedStringKeyGroup<T>(item.Key, item.BaseList));
                }
                return new ObservableCollection<AlphaGroupedStringKeyGroup<T>>(listOfCollections) as ObservableCollection<K>;
            }
            else
            {
                return null;
            }
        }

        public override void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<StringKeyGroup<T>> source)
        {
            SortedLocaleGrouping slg = new SortedLocaleGrouping(CultureInfo.CurrentCulture);
            foreach (StringKeyGroup<T> item in source)
            {
                int index = 0;
                
                try
                {
                    index = slg.GetGroupIndex(item.Key);
                }
                catch (NullReferenceException)
                {
                    
                }
                
                if (index >= 0 && index < collection.Count)
                {

                    if (collection[index].Any(s => s.Key == item.Key))
                    {
                        var subkey = collection[index].First(s => s.Key == item.Key);
                        foreach (T subItem in item)
                        {
                            subkey.Add(subItem);
                        }
                    }
                    else
                    {
                        InsertByAlpha(collection[index] as AlphaGroupedStringKeyGroup<T>, item);
                    }
                    
                    
                }
            }
        }

        public override List<StringKeyGroup<T>> BuildNestedCollection(IEnumerable<T> source)
        {
            return _subKey.BuildList(source);
        }

        public void InsertByAlpha(AlphaGroupedStringKeyGroup<T> keyGroup, StringKeyGroup<T> KeyGroupToInsert)
        {
            var ci = CultureInfo.CurrentCulture.CompareInfo;
            int indexToInsert = -1;
            for (int i = 0; i < keyGroup.Count; i++)
            {

                int j = ci.Compare(keyGroup[i].Key, KeyGroupToInsert.Key);
                if (j > 0)
                {
                    indexToInsert = i;
                    break;
                }
            }
            if (indexToInsert >= 0 && indexToInsert < keyGroup.Count)
            {
                keyGroup.Insert(indexToInsert, KeyGroupToInsert);
            }
            else
            {
                keyGroup.Add(KeyGroupToInsert);
            }
        }


        public override Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<StringKeyGroup<T>> source)
        {
            return Task.Run(() =>
            {
                SortedLocaleGrouping slg = new SortedLocaleGrouping(CultureInfo.CurrentCulture);
                foreach (StringKeyGroup<T> item in source)
                {
                    int index = 0;
                   
                    try
                    {
                        index = slg.GetGroupIndex(item.Key);
                    }
                    catch (NullReferenceException)
                    {
                       
                    }

                    if (index >= 0 && index < collection.Count)
                    {

                        if (collection[index].Any(s => s.Key == item.Key))
                        {
                            var subkey = collection[index].First(s => s.Key == item.Key);
                            foreach (T subItem in item)
                            {
                                UIThread.Invoke(()=> subkey.Add(subItem));
                            }
                        }
                        else
                        {
                            UIThread.Invoke(()=> InsertByAlpha(collection[index] as AlphaGroupedStringKeyGroup<T>, item));
                        }


                    }
                }
            });
        }
    }
}
