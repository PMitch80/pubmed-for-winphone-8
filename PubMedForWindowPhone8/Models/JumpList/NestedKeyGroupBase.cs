﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public abstract class NestedKeyGroupBase<K, T> : KeyGroupBase<K> where K : KeyGroupBase<T>
    {
        public abstract List<K> BuildNestedCollection(IEnumerable<T> source);

        public NestedKeyGroupBase()
            : base()
        {
        }

        public NestedKeyGroupBase(IEnumerable<K> list)
            : base(list)
        {
        }
    }
}
