﻿using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class YearWithDecadesKeyGroup<T> : KeyGroupBase<T>
    {
        public delegate DateTime?  GetKeyDelegate(T item);
        private GetKeyDelegate _keyDelegate;
        public int Year { get { return _numericalKey; } }
        //This is the last decade to be grouped Format eg 1980s -> 198
        private int _lastDecadeGrouped; 
        private int _firstDecadeAvailable;
        private bool _isDecade;

        private YearWithDecadesKeyGroup(int year, bool isDecade = false)
        {
            SetYear(year, isDecade);
        }

        

        private YearWithDecadesKeyGroup(int year, List<T> list, bool isDecade = false) : base (list)
        {
            SetYear(year, isDecade);
        }

        public YearWithDecadesKeyGroup(GetKeyDelegate keyDelegate, int latestDecadeToGroup = 0, int firstDecadeAvailable = 197)
        {
            
            _keyDelegate = keyDelegate;
            _lastDecadeGrouped  = latestDecadeToGroup;
            _firstDecadeAvailable = firstDecadeAvailable;
            
        }
        
        private void SetYear(int year, bool isDecade)
        {
            _isDecade = isDecade;
            
            if (isDecade & year != 0)
            {
                year = year / 10;
                _numericalKey = year * 10;
                _key = year +"0s";
            }
            else
            {
                _numericalKey = year;
                if (Year == 0)
                { _key = "..."; }
                else
                {
                    _key = Year.ToString();
                }
            }
        }

        private List<YearWithDecadesKeyGroup<T>> BuildKeyGroupList()
        {
            List<YearWithDecadesKeyGroup<T>> result = new List<YearWithDecadesKeyGroup<T>>();
            if(_lastDecadeGrouped ==0)
            {
                for (int i = DateTime.Now.Year; i >= _firstDecadeAvailable * 10; i--)
                {
                    result.Add(new YearWithDecadesKeyGroup<T>(i));
                }
                result.Add(new YearWithDecadesKeyGroup<T>(0));
            }
            else
            {
                for (int i = DateTime.Now.Year; i >= (_lastDecadeGrouped +1) * 10; i--)
                {
                    result.Add(new YearWithDecadesKeyGroup<T>(i));
                }
                for (int i = _lastDecadeGrouped; i >= _firstDecadeAvailable; i--)
                {
                    result.Add(new YearWithDecadesKeyGroup<T>(i*10, true));
                }
                result.Add(new YearWithDecadesKeyGroup<T>(0));
            }

            return result;
        }

        private int GetIndex(IList<YearWithDecadesKeyGroup<T>> collection, T item)
        {
            DateTime? date = _keyDelegate(item);
            if(!date.HasValue)
            {
                return collection.Count() -1;
            }
            else
            {
                int year = date.Value.Year;
                if (year < _firstDecadeAvailable*10)
                {
                    return collection.Count() -1;
                }
                if( year < (_lastDecadeGrouped + 1) *10)
                {
                    int decade = (year / 10)*10;
                    return collection.IndexOf((from s in collection where s.NumericalKey == decade select s).Single());
                }
                else
                {
                    return collection.IndexOf((from s in collection where s.NumericalKey == year select s).Single());
                }
            }
        }

        public override ObservableCollection<K> BuildCollection<K>(IEnumerable<T> source) 
        {
            
            if (typeof(K) == (typeof(YearWithDecadesKeyGroup<T>)))
            {
                List<YearWithDecadesKeyGroup<T>> listResults = BuildKeyGroupList();
                foreach (T item in source)
                {
                    DateTime? itemDate = _keyDelegate(item);
                    int year = 0;
                    if (itemDate.HasValue)
                    {
                        year = itemDate.Value.Year;
                    }
                    int i = GetIndex(listResults, item);
                    if (i>=0 && i < listResults.Count)
                    {
                        listResults[i].BaseList.Add(item);
                    }
                }


                List<YearWithDecadesKeyGroup<T>> collectionResults = new List<YearWithDecadesKeyGroup<T>>();
                foreach (YearWithDecadesKeyGroup<T> item in listResults)
                {
                    collectionResults.Add(new YearWithDecadesKeyGroup<T>(item._numericalKey, item.BaseList, item._isDecade));
                }
                
                ObservableCollection<K> collection = new ObservableCollection<YearWithDecadesKeyGroup<T>>(collectionResults) as ObservableCollection<K>;
                return collection;
            }
            return null;
        }

        public override void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            ObservableCollection<YearWithDecadesKeyGroup<T>> list = collection as ObservableCollection<YearWithDecadesKeyGroup<T>>;
            if (list != null)
            {
                foreach (T item in source)
                {
                    DateTime? itemDate = _keyDelegate(item);
                    int year = 0;
                    if (itemDate.HasValue)
                    {
                        year = itemDate.Value.Year;
                    }
                    int i = GetIndex(list, item);
                    if (i >= 0 && i < list.Count)
                    {
                        list[i].Add(item);
                    }
                }
            }
        }

        public override Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            ObservableCollection<YearWithDecadesKeyGroup<T>> list = collection as ObservableCollection<YearWithDecadesKeyGroup<T>>;
            if (list != null)
            {
                return Task.Run(() =>
                    {

                        foreach (T item in source)
                        {
                            DateTime? itemDate = _keyDelegate(item);
                            int year = 0;
                            if (itemDate.HasValue)
                            {
                                year = itemDate.Value.Year;
                            }
                            int i = GetIndex(list, item);
                            if (i >= 0 && i < list.Count)
                            {
                                UIThread.Invoke(() => list[i].Add(item));
                            }
                        }
                    });
            }
            else
            {
                return Task.Run(()=>{});
            }
        }
    }
}
