﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public abstract class KeyGroupBase<T> : ObservableCollection<T> 
    {
        public string Key { get { return _key; } }
        public int NumericalKey { get { return _numericalKey; } }
        public List<T> BaseList { get; set; }
        protected string _key;
        protected int _numericalKey;

        public KeyGroupBase()
        {
            BaseList = new List<T>();
        }
        public KeyGroupBase(IEnumerable<T> list) : base(list)
            
        {
            BaseList = list.ToList();
        }


        public abstract void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<T> source) where K : KeyGroupBase<T>;

        public abstract Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<T> source) where K : KeyGroupBase<T>;

        public abstract ObservableCollection<K> BuildCollection<K>(IEnumerable<T> source) where K :KeyGroupBase<T>;
    }
}
