﻿using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class JumpListCollection<K, T> :UIThreadNotifier where K : KeyGroupBase<T>
    {
        private ObservableCollection<K> _collection;
        
        public ObservableCollection<K> Collection 
        { 
            get {return _collection;}
            private set { this.SetProperty(ref this._collection, value); }
        }

        public int Count 
        {
            get 
            {
                if (Collection != null)
                    return Collection.Sum(k => k.Count);
                else
                    return 0;
            }
        }

        public List<T> Source { get; set; }

        private K _type; 

        public JumpListCollection(K key)
        {
            _type = key;
        }

        public void Rebuild()
        {
            if (Source != null)
            {
                lock (Source)
                {
                    if (Source.Count != Count)
                    {
                        BuildCollection(Source);
                    }
                }
            }
        }

        public async Task AddToList(List<T> additionalResults)
        {
           await _type.AddToCollectionAsync(Collection, additionalResults);
        }

        public void Build(List<T> source)
        {
            Source = source;

            BuildCollection(source);
        }

        private void BuildCollection(List<T> source)
        {
            
            Collection = _type.BuildCollection<K>(source);
        }
    }
}
