﻿using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class StringKeyGroup<T> : KeyGroupBase<T>
    {
        public delegate string GetKeyDelegate(T item);

        private GetKeyDelegate _keyDelegate; 

        /// <summary>
        /// private constructor.
        /// </summary>
        /// <param name="key">The key for this group.</param>
        private StringKeyGroup(string key)
        {
            _key = key;
        }

        public StringKeyGroup(GetKeyDelegate keyDelegate)
        {
            _keyDelegate = keyDelegate;
        }

        public List<StringKeyGroup<T>> BuildList(IEnumerable<T> list)
        {
            List<StringKeyGroup<T>> result = new  List<StringKeyGroup<T>>();
            foreach(T item in list)
            {
                string s = _keyDelegate(item);
                if(string.IsNullOrWhiteSpace(s))
                {
                    s = "...";
                }

                if (result.Any(r => r.Key == s))
                {
                    StringKeyGroup<T> keyGroupItem = result.First(r => r.Key == s);
                    keyGroupItem.Add(item);
                }
                else
                {
                    List<T> newlist = new List<T>();
                    StringKeyGroup<T> keyGroupItem = new StringKeyGroup<T>(s);
                    keyGroupItem.Add(item);
                    result.Add(keyGroupItem);
                }
            }
            result = result.OrderBy(m => m.Key).ToList();
            return result;
        }

        public override ObservableCollection<K> BuildCollection<K>(IEnumerable<T> source)
        {
            if (_keyDelegate != null)
            {
                return new ObservableCollection<StringKeyGroup<T>>(BuildList(source)) as ObservableCollection<K>;
            }
            else
            {
                throw new ArgumentNullException(); 
            }
        }

        public override void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            
            foreach (T item in source)
            {
                string s = _keyDelegate(item);
                if (string.IsNullOrWhiteSpace(s))
                {
                    s = "...";
                }

                if (collection.Any(r => r.Key == s))
                {
                    KeyGroupBase<T> keyGroupItem = collection.First(r => r.Key == s);
                    keyGroupItem.Add(item);
                }
                else
                {
                    List<T> newlist = new List<T>();
                    K keyGroupItem = new StringKeyGroup<T>(s) as K;
                    keyGroupItem.Add(item);
                    collection.Add(keyGroupItem);
                    collection.OrderBy(ol => ol.Key);
                }
            }
        }

        public override Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            return Task.Run(()=>
                {
                    foreach (T item in source)
                    {
                        string s = _keyDelegate(item);
                        if (string.IsNullOrWhiteSpace(s))
                        {
                            s = "...";
                        }

                        if (collection.Any(r => r.Key == s))
                        {
                            KeyGroupBase<T> keyGroupItem = collection.First(r => r.Key == s);
                            UIThread.Invoke(()=> keyGroupItem.Add(item));
                        }
                        else
                        {
                            List<T> newlist = new List<T>();
                            K keyGroupItem = new StringKeyGroup<T>(s) as K;
                            keyGroupItem.Add(item);
                            UIThread.Invoke(() =>
                                {
                                    collection.Add(keyGroupItem);
                                    collection.OrderBy(ol => ol.Key);
                                });
                        }
                    }
                });
        }
    }
}
