﻿using Microsoft.Phone.Globalization;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class AlphaKeyGroup<T> : KeyGroupBase<T>
    {
        /// <summary>
        /// The delegate that is used to get the key information.
        /// </summary>
        /// <param name="item">An object of type T</param>
        /// <returns>The key value to use for this object</returns>
        public delegate string GetKeyDelegate(T item);

        private GetKeyDelegate _keyDelegate;

        private AlphaKeyGroup(string key)
        {
            _key = key;
        }

        private AlphaKeyGroup(string key, IEnumerable<T> list): base(list)
        {
            _key = key;
        }

        public AlphaKeyGroup(GetKeyDelegate keyDelgate)
        {
            _keyDelegate = keyDelgate;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="slg">The </param>
        /// <returns>Theitems source for a LongListSelector</returns>
        private static List<AlphaKeyGroup<T>> CreateGroups(SortedLocaleGrouping slg)
        {
            List<AlphaKeyGroup<T>> list = new List<AlphaKeyGroup<T>>();

            foreach (string key in slg.GroupDisplayNames)
            {
                AlphaKeyGroup<T> keygroup = new AlphaKeyGroup<T>(key);
                keygroup.BaseList = new List<T>();
                list.Add(keygroup);
            }

            return list;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="items">The items to place in the groups.</param>
        /// <param name="ci">The CultureInfo to group and sort by.</param>
        /// <param name="getKey">A delegate to get the key from an item.</param>
        /// <param name="sort">Will sort the data if true.</param>
        /// <returns>An items source for a LongListSelector</returns>
        public List<AlphaKeyGroup<T>> BuildList(IEnumerable<T> items, bool sort)
        {
            CultureInfo ci = CultureInfo.CurrentCulture;
            SortedLocaleGrouping slg = new SortedLocaleGrouping(ci);
            List<AlphaKeyGroup<T>> list = CreateGroups(slg);

            foreach (T item in items)
            {
                int index = 0;

                try
                {
                    index = slg.GetGroupIndex(_keyDelegate(item));
                }
                catch(NullReferenceException)
                {
                }
                
                if (index >= 0 && index < list.Count)
                {
                    list[index].BaseList.Add(item);
                }
            }

            if (sort)
            {
                foreach (AlphaKeyGroup<T> group in list)
                {
                    group.BaseList.Sort((c0, c1) => { return ci.CompareInfo.Compare(_keyDelegate(c0), _keyDelegate(c1)); });
                }
            }

            return list;
        }

        public override ObservableCollection<K> BuildCollection<K>(IEnumerable<T> source)
        {
            if (typeof(K) == (typeof(AlphaKeyGroup<T>)))
            {
                List<AlphaKeyGroup<T>> list = BuildList(source, true);
                List<AlphaKeyGroup<T>> collection = new List<AlphaKeyGroup<T>>();
                foreach (AlphaKeyGroup<T> item in list)
                {
                    collection.Add(new AlphaKeyGroup<T>(item.Key, item.BaseList));
                }
                ObservableCollection<K> result = new ObservableCollection<AlphaKeyGroup<T>>(collection) as ObservableCollection<K>;
                return result;
            }
            return null;
        }

        public override void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            SortedLocaleGrouping slg = new SortedLocaleGrouping(CultureInfo.CurrentCulture);
            foreach (T item in source)
            {
                int index = 0;
                try
                {
                    index = slg.GetGroupIndex(_keyDelegate(item));
                }
                catch (NullReferenceException)
                {
                }
                
                if (index >= 0 && index < collection.Count)
                {
                    collection[index].Add(item);
                }
            }
        }


        public override Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            return Task.Run(() =>
            {
                SortedLocaleGrouping slg = new SortedLocaleGrouping(CultureInfo.CurrentCulture);
                foreach (T item in source)
                {
                    int index = 0;
                    
                    try
                    {
                        index = slg.GetGroupIndex(_keyDelegate(item));
                    }
                    catch (NullReferenceException)
                    {
                       
                    }

                    if (index >= 0 && index < collection.Count)
                    {
                        UIThread.Invoke(()=> collection[index].Add(item));
                    }
                }
            });
        }
    }
}

