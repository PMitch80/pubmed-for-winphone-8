﻿using Microsoft.Phone.Globalization;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.Models
{
    public class YearKeyGroup<T> : KeyGroupBase<T>
    {
        public delegate DateTime?  GetKeyDelegate(T item);
        private GetKeyDelegate _keyDelegate;
        public int Year { get { return _numericalKey; } }

        private YearKeyGroup(int year)
        {
            SetYear(year);
        }

        private YearKeyGroup(int year, List<T> list) : base (list)
        {
            SetYear(year);
        }

        public YearKeyGroup(GetKeyDelegate keyDelegate)
        {
            _keyDelegate = keyDelegate;
        }

        private void SetYear(int year)
        {
            _numericalKey = year;
            if (Year == 0)
            { _key = "..."; }
            else
            {
                _key = Year.ToString();
            }
        }

        private void AddItemToList(List<YearKeyGroup<T>> list, T item, int year)
        {
            if (list.Any(s => s.Year == year))
            {
                YearKeyGroup<T> keyGroupItem = list.First(m => m.Year == year);
                keyGroupItem.BaseList.Add(item);
            }
            else
            {
                YearKeyGroup<T> keyGroupItem = new YearKeyGroup<T>(year);
                keyGroupItem.BaseList = new List<T>();
                keyGroupItem.BaseList.Add(item);
                list.Add(keyGroupItem);
            }

        }

        private void AddItemToCollection(ObservableCollection<KeyGroupBase<T>> keyGroup, T item, int year)
        {
            
                if (keyGroup.Any(s => s.NumericalKey == year))
                {
                    KeyGroupBase<T> keyGroupItem = keyGroup.First(s => s.NumericalKey == year) as YearKeyGroup<T>;
                    keyGroupItem.Add(item);
                }
                else
                {
                    List<T> newlist = new List<T>();
                    newlist.Add(item);
                    YearKeyGroup<T> keyGroupItem = new YearKeyGroup<T>(year);
                    keyGroup.Add(keyGroupItem);
                }
            
            
        }

        public void OrderByYear(ObservableCollection<KeyGroupBase<T>> keyGroup)
        {
            for (int i = 0; i < keyGroup.Count; i++)
            {
                bool passed = false;
                if (i + 1 < keyGroup.Count)
                {
                    while (!passed)
                    {

                        for (int x = i + 1; x < keyGroup.Count; x++)
                        {
                            if (keyGroup[i].NumericalKey < keyGroup[x].NumericalKey)
                            {
                                keyGroup.Move(x, i);
                                break;
                            }
                            if (x + 1 == keyGroup.Count)
                            {
                                passed = true;
                            }
                        }
                    }
                }
            }
        }


        public override ObservableCollection<K> BuildCollection<K>(IEnumerable<T> source)
        {

            List<YearKeyGroup<T>> listResults = new List<YearKeyGroup<T>>();
            foreach (T item in source)
            {
                DateTime? itemDate = _keyDelegate(item);
                if (itemDate.HasValue)
                {
                    int year = itemDate.Value.Year;
                    AddItemToList(listResults, item, year);
                }
                else
                {
                    int year = 0;
                    AddItemToList(listResults, item, year);
                }
            }
            listResults = listResults.OrderByDescending(m => m.Year).ToList();
            List<YearKeyGroup<T>> collectionResults = new List<YearKeyGroup<T>>();
            foreach (YearKeyGroup<T> item in listResults)
            {
                collectionResults.Add(new YearKeyGroup<T>(item._numericalKey, item.BaseList));
            }

            return new ObservableCollection<KeyGroupBase<T>>(collectionResults) as ObservableCollection<K>;
        }

        public override void AddToCollection<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            ObservableCollection<KeyGroupBase<T>> castCollection = collection as ObservableCollection<KeyGroupBase<T>>;
            foreach (T item in source)
            {
                DateTime? itemDate = _keyDelegate(item);
                if (itemDate.HasValue)
                {
                    int year = itemDate.Value.Year;
                    AddItemToCollection(castCollection, item, year);
                }
                else
                {
                    int year = 0;
                    AddItemToCollection(castCollection, item, year);
                }
            }
            OrderByYear(castCollection);
        }


        public override Task AddToCollectionAsync<K>(ObservableCollection<K> collection, IEnumerable<T> source)
        {
            return Task.Run(() =>
            {
                ObservableCollection<KeyGroupBase<T>> castCollection = collection as ObservableCollection<KeyGroupBase<T>>;
                foreach (T item in source)
                {
                    DateTime? itemDate = _keyDelegate(item);
                    if (itemDate.HasValue)
                    {
                        int year = itemDate.Value.Year;
                        UIThread.Invoke(() => AddItemToCollection(castCollection, item, year));
                    }
                    else
                    {
                        int year = 0;
                        UIThread.Invoke(() => AddItemToCollection(castCollection, item, year));
                    }
                }
                UIThread.Invoke(() => OrderByYear(castCollection));
            });
        }
    }
}
