﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;

namespace PubMedLite.UserControls
{
    public partial class TextInputPopUp : UserControl
    {
        public TextInputPopUp()
        {
            InitializeComponent();
            textbox.LostFocus += TextBoxFocusOut;
        }

        private double _originalParentOpacity;


        UIElement _parent { get { return this.Parent as UIElement; } }

        private void TextBoxFocusOut(object sender, RoutedEventArgs e)
        {
            Dismiss();
        }

        double ScreenHeight { get { return App.Current.Host.Content.ActualHeight; } }

        double ScreenWidth { get { return App.Current.Host.Content.ActualWidth; } }

        public void Show()
        {
            if (!LayoutRoot.IsOpen)
            {
                LayoutRoot.IsOpen = true;
                textbox.Focus();
                _originalParentOpacity = _parent.Opacity;
                _parent.Opacity = .5;
            }
        }

        public void Show(string textBoxValue)
        {
            textbox.Text = textBoxValue;
            Show();
        }

        public void Dismiss()
        {
            LayoutRoot.IsOpen = false;
            _parent.Opacity = _originalParentOpacity;
        }



        public event EventHandler OnTextInput;

        private void EnterKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Dismiss();
                if(OnTextInput != null)
                {
                    OnTextInput(textbox.Text, new EventArgs());
                }
            }
        }

    }
}
