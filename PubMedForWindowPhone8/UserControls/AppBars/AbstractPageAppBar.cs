﻿using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using PubMedLite.Presenters;
using PubMedLite.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace PubMedLite.UserControls
{
    public class AbstractPageAppBar :AppBarBase
    {
        private ApplicationBarIconButton _add;
        private ApplicationBarIconButton _removeButton;
        private ApplicationBarIconButton _home;
        private ApplicationBarIconButton _pubmed;
        private bool _isOnReadingList;
        public bool IsOnReadingList { get {return _isOnReadingList;} set { _isOnReadingList = value; SwitchAddRemove(value);}}
        private AbstractPagePresenter Presenter { get { return _currentPresenter as AbstractPagePresenter; } }

        private void SwitchAddRemove(bool value)
        {
            if (value)
            {
                ApplicationBar.Buttons.Remove(_add);
                if (!ApplicationBar.Buttons.Contains(_removeButton))
                {
                    ApplicationBar.Buttons.Add(_removeButton);
                }
            }
            else
            {
                ApplicationBar.Buttons.Remove(_removeButton);
                if (!ApplicationBar.Buttons.Contains(_add))
                {
                    ApplicationBar.Buttons.Add(_add);
                }
            }
        }

        public AbstractPageAppBar(AbstractPagePresenter currentPesenter)
            : base(currentPesenter, false)
        {

            _pubmed = new ApplicationBarIconButton();
            _pubmed.Text = AppResources.ToPubMedButton;
            _pubmed.IconUri = new Uri("/Assets/AppBar/DocIcon.png", UriKind.Relative);
            _pubmed.Click += PubmedPressed;
            ApplicationBar.Buttons.Add(_pubmed);
            
            _home = new ApplicationBarIconButton();
            _home.Text = AppResources.HomeButtonText;
            _home.IconUri = new Uri("/Assets/AppBar/home.png", UriKind.Relative);
            _home.Click += HomePressed;
            ApplicationBar.Buttons.Add(_home);

            _add = new ApplicationBarIconButton();
            _add.Text = AppResources.AddArticleToReadingListButton;
            _add.IconUri = new Uri("/Assets/AppBar/unfavs.png", UriKind.Relative);
            _add.Click += AddPressed;
                

            _removeButton = new ApplicationBarIconButton();
            _removeButton.Text = AppResources.RemoveItemButtonText;
            _removeButton.IconUri = new Uri("/Assets/AppBar/favs.png", UriKind.Relative);
            _removeButton.Click += RemovePressed;
        }

        private void PubmedPressed(object sender, EventArgs e)
        {
            string s = "http://www.ncbi.nlm.nih.gov/pubmed/" + Presenter.PMId;
            WebBrowserTask toPubmedTask = new WebBrowserTask();
            toPubmedTask.Uri = new Uri(s, UriKind.Absolute);
            toPubmedTask.Show();
        }

        private void HomePressed(object sender, EventArgs e)
        {
            Presenter.Page.NavigationService.Navigate(new Uri("/Pages/MainPage.xaml", UriKind.Relative));
        }

        private async void AddPressed(object sender, EventArgs e)
        {
            await Presenter.AddToReadingList();
        }

        private async void RemovePressed(object sender, EventArgs e)
        {
            await Presenter.RemoveFromReadingList();
        }
        
    }
}
