﻿using Microsoft.Phone.Shell;
using PubMedLite.Presenters;
using PubMedLite.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.UserControls
{
    public class MainPageAppBar : AppBarBase
    {

        private MainPage.MainPageSubpage CurrentSubpage { get { return Presenter.SelectedPage; } }
        private ApplicationBarIconButton _advanceSearch;
        private ApplicationBarIconButton _bookmarkSearch;
        private ApplicationBarIconButton _addTile;
        private ApplicationBarIconButton _removeItem;
        private ApplicationBarIconButton _rename;
        private bool isSearchItemSelected { get { return Presenter.SelectedSummary != null; } }
        private bool isSpotlightItemSelected { get { return Presenter.SelectedSpotlightItem != null; } }
        private bool isSearchQuerySelected { get { return Presenter.SelectedSearch != null; } }
        private bool isReadingListItemSelected { get { return Presenter.ReadingList.Any(a => a.IsSelected); } }


        public MainPagePresenter Presenter { get { return base._currentPresenter as MainPagePresenter; } }

        public MainPageAppBar(MainPagePresenter currentPresenter)
            : base(currentPresenter)
        {
            _advanceSearch = new ApplicationBarIconButton();
            _advanceSearch.Text = AppResources.AdvanceSeachButtonText;
            _advanceSearch.IconUri = new Uri("/Assets/AppBar/feature.search.png", UriKind.Relative);
            _advanceSearch.Click += AdvanceSearchClicked;

            _bookmarkSearch = new ApplicationBarIconButton();
            _bookmarkSearch.Text = AppResources.AddToFavouriteSearchButtonText ;
            _bookmarkSearch.IconUri = new Uri("/Assets/AppBar/favs.png", UriKind.Relative);
            _bookmarkSearch.Click += BookMarkClicked;
            _bookmarkSearch.IsEnabled = isSearchQuerySelected;
            Presenter.Page.RecentSearchesList.SelectionChanged += SelectedSearchQueryChanged;

            _addTile = new ApplicationBarIconButton();
            _addTile.Text = AppResources.AddFavouriteSeachToTileButtonText;
            _addTile.IconUri =  new Uri("/Assets/AppBar/favs.png", UriKind.Relative);
            _addTile.IsEnabled = isSpotlightItemSelected;
            _addTile.Click += PinSpotLightItem;

            _removeItem = new ApplicationBarIconButton();
            _removeItem.Text = AppResources.RemoveItemButtonText;
            _removeItem.IconUri = new Uri("/Assets/AppBar/minus.png", UriKind.Relative);
            _removeItem.Click += RemoveClicked;
            _removeItem.IsEnabled = isSpotlightItemSelected;
            Presenter.Page.SpotlightList.SelectionChanged += SpotlightSelectionChanged;

            _rename = new ApplicationBarIconButton();
            _rename.Text = AppResources.RenameSearchButtonText;
            _rename.IconUri = new Uri("/Assets/AppBar/Aa.png", UriKind.Relative);
            _rename.Click += RenameClicked;
            _rename.IsEnabled = isSpotlightItemSelected;

            SetApplicationBar();
            Presenter.Page.LayoutRoot.SelectionChanged += SelectedPageChanged;
        }

        private void SetApplicationBar()
        {
            switch (CurrentSubpage)
            {
                case MainPage.MainPageSubpage.Search:
                    LoadSearchBar();
                    break;
                case MainPage.MainPageSubpage.Recent:
                    LoadRecentBar();
                    break;
                case MainPage.MainPageSubpage.SpotLight:
                    LoadSpotlightBar();
                    break;
                case MainPage.MainPageSubpage.ReadingList:
                    LoadReadingListBar();
                    break;
                default:
                    
                    break;
            }
        }

        #region Build Application Bars
        private void ClearButtons()
        {
            while(ApplicationBar.Buttons.Count >0) 
            {
                ApplicationBar.Buttons.RemoveAt(0);
            }
        }

        private void LoadSearchBar()
        {
           
            ApplicationBar.Buttons.Add(_bookmarkSearch);
            ApplicationBar.Buttons.Add(_advanceSearch);
        }

        private void LoadRecentBar()
        {
           
            ApplicationBar.Buttons.Add(_bookmarkSearch);
        }

        private void LoadSpotlightBar()
        {

           
            ApplicationBar.Buttons.Add(_removeItem);
            ApplicationBar.Buttons.Add(_addTile);
            ApplicationBar.Buttons.Add(_rename);
            _removeItem.IsEnabled = isSpotlightItemSelected;
            _addTile.IsEnabled = isSpotlightItemSelected;
            _rename.IsEnabled = isSpotlightItemSelected;
        }

        private void LoadReadingListBar()
        {
            if (isReadingListItemSelected)
            {
                Show();
            }
            ApplicationBar.Buttons.Add(_removeItem);
            _removeItem.IsEnabled = true;
        }

        #endregion

        #region Page Change Handlers

        private void SelectedPageChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Hide();
            ClearButtons();
            SetApplicationBar();
        }


        private void SpotlightSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _removeItem.IsEnabled = isSpotlightItemSelected;
            _addTile.IsEnabled = isSpotlightItemSelected;
            _rename.IsEnabled = isSpotlightItemSelected;
        }


        private void SelectedSearchQueryChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _bookmarkSearch.IsEnabled = isSearchQuerySelected;
        }


        #endregion




        #region Button Click Methods

        private void AdvanceSearchClicked(object sender, EventArgs e)
        {
            Presenter.NavigateToAdvanceSearch();
        }

        private async void BookMarkClicked(object sender, EventArgs e)
        {
            await Presenter.AddToSpotlight();
        }

        private async void RemoveClicked(object sender, EventArgs e)
        {
            if (CurrentSubpage == MainPage.MainPageSubpage.ReadingList)
            {
                await Presenter.RemoveSelectedFromReadingList();
                if (!isReadingListItemSelected)
                {
                    Hide();
                }
            }
            else if (CurrentSubpage == MainPage.MainPageSubpage.SpotLight)
            {
                await Presenter.RemoveFromSpotlight();
            }
        }

        private void RenameClicked(object sender, EventArgs e)
        {
            Presenter.OpenRenamgeDialog();
        }

        private void PinSpotLightItem(object sender, EventArgs e)
        {
            Presenter.AddSecondaryTile();
        }
        #endregion
        
    }
}
