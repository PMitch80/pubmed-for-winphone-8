﻿using Microsoft.Phone.Shell;
using PubMedLite.Presenters;
using PubMedLite.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.UserControls
{
    public class AdvanceSearchPageAppBar : AppBarBase
    {
        private ApplicationBarIconButton _home;
        private ApplicationBarIconButton _bookmarkSearch;
        
        AdvanceSearchPagePresenter Presenter { get { return _currentPresenter as AdvanceSearchPagePresenter; } }

        public AdvanceSearchPageAppBar(AdvanceSearchPagePresenter presenter)
            : base(presenter)
        {
            _bookmarkSearch = new ApplicationBarIconButton();
            _bookmarkSearch.Text = AppResources.AddToFavouriteSearchButtonText;
            _bookmarkSearch.IconUri = new Uri("/Assets/AppBar/favs.png", UriKind.Relative);
            _bookmarkSearch.Click += BookMarkClicked;
            _bookmarkSearch.IsEnabled = Presenter.CurrentQuery != null;
            ApplicationBar.Buttons.Add(_bookmarkSearch);

            _home = new ApplicationBarIconButton();
            _home.Text = AppResources.HomeButtonText;
            _home.IconUri = new Uri("/Assets/AppBar/home.png", UriKind.Relative);
            _home.Click += HomePressed;
            ApplicationBar.Buttons.Add(_home);
        }

        private async void BookMarkClicked(object sender, EventArgs e)
        {
            await Presenter.AddToSpotlight();
        }

        private void HomePressed(object sender, EventArgs e)
        {
            Presenter.Page.NavigationService.Navigate(new Uri("/Pages/MainPage.xaml", UriKind.Relative));
        }

        public void SetCurrentSearch()
        {
            _bookmarkSearch.IsEnabled = Presenter.CurrentQuery != null; 
        }
    }
}
