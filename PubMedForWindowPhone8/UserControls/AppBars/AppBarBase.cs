﻿using Microsoft.Phone.Shell;
using PubMedLite.Presenters;
using PubMedLite.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PubMedLite.UserControls
{
    public class AppBarBase
    {
        public ApplicationBar ApplicationBar { get; set; }
        protected PresenterBase _currentPresenter;

        public AppBarBase(PresenterBase currentPresenter, bool isMinimized = true)
        {
            _currentPresenter = currentPresenter;
            ApplicationBar = new ApplicationBar();
            
            if (isMinimized)
            {
                ApplicationBar.Mode = ApplicationBarMode.Minimized;
            }
            else
            {
                ApplicationBar.Mode = ApplicationBarMode.Default;
            }
            ApplicationBarMenuItem aboutMenu = new ApplicationBarMenuItem();
            aboutMenu.Text = AppResources.AboutMenuItem;
            aboutMenu.Click += aboutClick;
            ApplicationBar.MenuItems.Add(aboutMenu);
        }

        private void aboutClick(object sender, EventArgs e)
        {
            MessageBox.Show(AppResources.AboutText);
        }

        public void Show()
        {
            ApplicationBar.Mode = ApplicationBarMode.Default;
        }
        public void Hide()
        {
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
        }
        
    }
}
