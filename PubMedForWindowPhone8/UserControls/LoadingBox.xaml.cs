﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;

namespace PubMedLite.UserControls
{
    public partial class LoadingBox : UserControl
    {
        public LoadingBox()
        {
            InitializeComponent();
            userControl.Visibility = Visibility.Collapsed;
            MessageBox.Text = Message;
        }

        public static readonly DependencyProperty IsDisplayedProperty = DependencyProperty.Register("IsDisplayed", typeof(bool), typeof(LoadingBox), new PropertyMetadata(false, IsLoadingPropertyChanged));
        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register("Message", typeof(string), typeof(LoadingBox), new PropertyMetadata("Loading...", MessagePropertyChanged));

        private static void MessagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LoadingBox source = d as LoadingBox;
            if (source != null)
            {
                source.MessageBox.Text = source.Message;
            }
        }

        private static void IsLoadingPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LoadingBox source = d as LoadingBox;
            if (source != null)
            {
                if (source.IsDisplayed == true)
                {
                    source.userControl.Visibility = Visibility.Visible;
                }
                else
                {
                    source.userControl.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set
            {
                SetValue(MessageProperty, value);
            }
        }

        public bool IsDisplayed
        {
            get { return (bool)GetValue(IsDisplayedProperty); }
            set
            {
                SetValue(IsDisplayedProperty, value);
            }
        }
      
    }
}
