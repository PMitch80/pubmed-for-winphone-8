﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PubMedLite.UserControls
{
    public class BoolToAccentConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            App app = (App)App.Current;
            if ((bool)value)
            {
                return app.Resources["PhoneAccentBrush"] ;
            }
            else
            {
                return app.Resources["PhoneForegroundBrush"];
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
