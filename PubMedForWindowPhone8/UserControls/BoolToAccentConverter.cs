﻿using System;
using System.Windows.Data;

namespace PubMedLite.UserControls
{
    public class BoolToAccentTransparentConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            App app = (App)App.Current;
            if ((bool)value)
            {
                return app.Resources["PhoneAccentBrush"] ;
            }
            else
            {
                return app.Resources["TransparentBrush"];
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
