﻿using PubMedLite.Models;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PubMedLite.Presenters
{
    public abstract class PresenterBase : Notifier
    {
        protected UserControl _page;
        
        public ApplicationPresenter AppPresenter { get { return ApplicationPresenter.Current; } }

        public PresenterBase(UserControl page)
        {
            _page = page;
        }
    }
}
