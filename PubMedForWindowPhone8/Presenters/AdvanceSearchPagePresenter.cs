﻿using PubMedLite.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using PubMedLite.UserControls;
using PubMedLite.BLL;

namespace PubMedLite.Presenters
{
    public class AdvanceSearchPagePresenter : PresenterBase
    {
        public AdvanceSearchPage Page { get { return base._page as AdvanceSearchPage; } }

        public AdvanceSearchPage.AdvanceSearchSubpage Subpage { get { return (AdvanceSearchPage.AdvanceSearchSubpage)Page.LayoutRoot.SelectedIndex; } }

        private PubMedSearchService _search;

        private string _searchQuery;

        public string SearchQuery { get { return _searchQuery; } set { this.SetProperty(ref this._searchQuery, value); } }

        public Loader LoadingBox { get; set; }

        public JumpListCollection<YearWithDecadesKeyGroup<PubMedSummary>, PubMedSummary> YearSearch { get; set; }

        public NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary> FirstAuthorSearch { get; set; }

        public NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary> LastAuthorSearch { get; set; }

        public NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary> JournalSearch { get; set; }
        
        private List<PubMedSummary> _currentSearch;
        public List<PubMedSummary> CurrentSearch { get { return _currentSearch; } set { this.SetProperty(ref this._currentSearch, value); } }

        private PubMedSearch _currentQuery;
        public PubMedSearch CurrentQuery { get { return _currentQuery; } set { this.SetProperty(ref this._currentQuery, value); Page.AppBar.SetCurrentSearch(); } }

        public AdvanceSearchPagePresenter(AdvanceSearchPage page) :
            base(page)
        {
            LoadingBox = new Loader();
            _search = new PubMedSearchService(AppPresenter.Repository, LoadingBox);
            _search.NewSummariesAdded += AddNewSummaryHandler;
            JournalSearch = new NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary>(new AlphaGroupedStringKeyGroup<PubMedSummary>(new StringKeyGroup<PubMedSummary>((PubMedSummary s) => s.Source )));
            YearSearch = new JumpListCollection<YearWithDecadesKeyGroup<PubMedSummary>, PubMedSummary>(new YearWithDecadesKeyGroup<PubMedSummary>((PubMedSummary s) => { return s.PubDate; },199));
            FirstAuthorSearch = new NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary>(new AlphaGroupedStringKeyGroup<PubMedSummary>(new StringKeyGroup<PubMedSummary>((PubMedSummary s) => s.FirstAuthor)));
            LastAuthorSearch = new NestedJumpListCollection<AlphaGroupedStringKeyGroup<PubMedSummary>, StringKeyGroup<PubMedSummary>, PubMedSummary>(new AlphaGroupedStringKeyGroup<PubMedSummary>(new StringKeyGroup<PubMedSummary>((PubMedSummary s) => s.LastAuthor)));
        }

        public async Task LoadResult(int navigatedToId)
        {
            
            if (navigatedToId != 0)
            {
                PubMedSearchQuery query = await _search.GetSearchResultsAsync(navigatedToId);
                CurrentQuery = query.Search;
                CurrentSearch = query.Result.ToList();
                LoadJumpLists();
                await ExtendList();
            }
           
        }

        private async void AddNewSummaryHandler(object sender, EventArgs e)
        {
            List<PubMedSummary> resultsToAdd = (List<PubMedSummary>)sender;
            if (resultsToAdd != null)
            {
                await LoadResults(resultsToAdd);
            }
        }

        private void LoadJumpLists()
        {
            YearSearch.Build(CurrentSearch);
            FirstAuthorSearch.Build(CurrentSearch);
            LastAuthorSearch.Build(CurrentSearch);
            JournalSearch.Build(CurrentSearch);
        }

        public string GetSource(PubMedSummary s)
        {
            return s.Source;
        }

        public async Task Search()
        {
            if (!string.IsNullOrWhiteSpace(SearchQuery))
            {
                Task<PubMedSearchQuery> t = _search.SearchPubmedAsync(SearchQuery);
                Page.LayoutRoot.Focus();
                PubMedSearchQuery result = await t;
                if (result != null)
                {
                    await AppPresenter.AddToRecentSearchList(result.Search);
                    CurrentSearch = result.Result.ToList();
                    CurrentQuery = result.Search;
                    LoadJumpLists();
                    await ExtendList();
                }
            }
        }

        public async Task ExtendList()
        {
            await _search.GetAdditionalResultListAsync(CurrentQuery);
        }

        public async Task LoadResults(List<PubMedSummary> resultsToLoad)
        {
            CurrentSearch.AddRange(resultsToLoad);
             
            switch (Subpage)
            {
                case AdvanceSearchPage.AdvanceSearchSubpage.Year :
                    await YearSearch.AddToList(resultsToLoad);
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.FirstAuthor :
                    await FirstAuthorSearch.AddToList(resultsToLoad);
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.LastAuthor:
                    await LastAuthorSearch.AddToList(resultsToLoad);
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.Journal:
                    await JournalSearch.AddToList(resultsToLoad); 
                    break;

                default:
                    break;
            }
        }

        public void PageTurned()
        {
            switch (Subpage)
            {
                case AdvanceSearchPage.AdvanceSearchSubpage.Year:
                    YearSearch.Rebuild();
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.FirstAuthor:
                    FirstAuthorSearch.Rebuild();
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.LastAuthor:
                    LastAuthorSearch.Rebuild();
                    break;

                case AdvanceSearchPage.AdvanceSearchSubpage.Journal:
                    JournalSearch.Rebuild();
                    break;

                default:
                    break;
            }
        }

        public async Task AddToSpotlight()
        {
            await AppPresenter.AddToSpotlight(CurrentQuery);
        }



    }
}
