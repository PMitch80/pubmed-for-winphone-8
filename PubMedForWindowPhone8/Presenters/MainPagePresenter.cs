﻿using Microsoft.Phone.Shell;
using PubMedLite.Models;
using PubMedLite.UserControls;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PubMedLite.Presenters
{
   
    public class MainPagePresenter : PresenterBase
    {
        #region Properties

        public MainPage Page { get { return base._page as MainPage; } }
        
        public PubMedSearchService _search;
        
        

        private bool isLoadingNewSearch;

        private string _searchQuery;

        public string SearchQuery { get { return _searchQuery; } set { this.SetProperty(ref this._searchQuery, value); } }
        
        public PubMedSummary SelectedSummary { get { return Page.SearchResult.SelectedItem as PubMedSummary; } }
        
        public PubMedSummary SelectedReadinglistItem { get { return Page.ReadingListBox.SelectedItem as PubMedSummary; } }
        
        public PubMedSearch SelectedSearch { get { return Page.RecentSearchesList.SelectedItem as PubMedSearch; } }
        
        public PubMedSearch SelectedSpotlightItem { get { return Page.SpotlightList.SelectedItem as PubMedSearch; } }

       
        public MainPage.MainPageSubpage SelectedPage { get { return (MainPage.MainPageSubpage)Page.LayoutRoot.SelectedIndex; } }
        
        private PubMedSearch _openSearch;
       
        public PubMedSearch OpenSearch { get { return _openSearch; } set { _openSearch = value;  } }

        public ObservableCollection<PubMedSearch> RecentSearches
        {
            get
            {
                return AppPresenter.RecentSearches;
            }
        }

        public ObservableCollection<PubMedSummary> ReadingList
        {
            get
            {
                return AppPresenter.ReadingList;
            }
        }

        private ObservableCollection<PubMedSummary> _currentSearch;
        
        public ObservableCollection<PubMedSummary> CurrentSearch 
        {
            get { return _currentSearch; }
            set { this.SetProperty(ref this._currentSearch, value); }
        }

        public ObservableCollection<PubMedSearch> SpotLightSearches
        {
            get
            {
                return AppPresenter.SpotLightSearches;
            }
        }

        public MainPageLoader LoadingBar {get; set;}

        

        #endregion

        public MainPagePresenter(MainPage page) :
            base(page)
        {
            
            LoadingBar = new MainPageLoader();
            _search = new PubMedSearchService(AppPresenter.Repository, LoadingBar);
            SearchQuery = "";
        }

        public async Task Search(string query)
        {
            isLoadingNewSearch = true;
            PubMedSearchQuery result = await _search.SearchPubmedAsync(query);
            if (result.Search != null)
            {
                CurrentSearch = new ObservableCollection<PubMedSummary>(result.Result);
                await AppPresenter.AddToRecentSearchList(result.Search);
                OpenSearch = result.Search;
            }
            isLoadingNewSearch = false;
        }

       public async Task LoadSearchResult(int searchId)
        {
            if (!isLoadingNewSearch)
            {
                if (searchId != 0)
                {
                    PubMedSearchQuery query = await _search.GetSearchResultsAsync(searchId);
                    CurrentSearch = new ObservableCollection<PubMedSummary>(query.Result);
                    SearchQuery = query.Search.SearchString;
                    OpenSearch = query.Search;
                }
            }
            
        }

        public async Task AddToSpotlight()
        {
            await AppPresenter.AddToSpotlight(SelectedSearch);
        }

        public void AddSecondaryTile()
        {
            AppPresenter.AddSecondaryTile(SelectedSpotlightItem);
        }

        public async Task RemoveFromSpotlight()
        {
            await AppPresenter.RemoveFromSpotlight(SelectedSpotlightItem);
        }

        public void NavigateToAdvanceSearch()
        {
            int id = 0;

            if (OpenSearch != null)
            {
                id = OpenSearch.Id;
            }
            if (SelectedSearch != null && SelectedPage == MainPage.MainPageSubpage.Recent)
            {
                id = SelectedSearch.Id;
            }
            if (SelectedSpotlightItem != null && SelectedPage == MainPage.MainPageSubpage.SpotLight)
            {
                id = SelectedSpotlightItem.Id;
            }
            
            Page.NavigationService.Navigate(new Uri("/Pages/AdvanceSearchPage.xaml?id=" + id, UriKind.Relative));
        }

        public async Task PageTurned()
        {
            if (SelectedSpotlightItem != null)
            {
                if (OpenSearch != SelectedSpotlightItem)
                {
                    OpenSearch = SelectedSpotlightItem;
                    Page.ClearSelectedSearchs();
                    ClearSearchSelections();
                    await LoadSearchResult(OpenSearch.Id);
                }
            }
            else if (SelectedSearch != null)
            {
                if (OpenSearch != SelectedSearch)
                {
                    OpenSearch = SelectedSearch;
                    Page.ClearSelectedSearchs();
                    ClearSearchSelections();
                    await LoadSearchResult(OpenSearch.Id);
                }
            }
           
            
        }

        public void OpenRenamgeDialog()
        {
            Page.PopUp();
        }

        public async Task RemoveSelectedFromReadingList()
        {
            while( ReadingList.Any(m=>m.IsSelected))
            {
                PubMedSummary item = ReadingList.FirstOrDefault(m => m.IsSelected);
                if (item != null)
                {
                    await AppPresenter.RemoveFromReadingList(item);
                }
            }
        }

        public void ClearSearchSelections()
        {
            foreach (PubMedSearch item in RecentSearches)
            {
                item.IsSelected = false;
            }
            foreach (PubMedSearch item in SpotLightSearches)
            {
                item.IsSelected = false;
            }
        }
    }
}
