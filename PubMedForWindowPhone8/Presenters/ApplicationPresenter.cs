﻿using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using PubMedLite.Models;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PubMedLite.Presenters
{
    public class ApplicationPresenter : IDisposable
    {
        private Application _app;
        public App App {get {return _app as App;}}
        private DataContext _appDb;
        public AppDbContext AppDb { get { return _appDb as AppDbContext; } }
        public ObservableCollection<PubMedSearch> RecentSearches { get; set; }
        public ObservableCollection<PubMedSearch> SpotLightSearches { get; set; }
        public ObservableCollection<PubMedSummary> ReadingList { get; set; }
        public AppDbRepository Repository;
        public int RecentSearchListMaxContent { get; set; }

        public static ApplicationPresenter Current
        {
            get
            {
                App _currentApp = (App)App.Current;
                return _currentApp.Presenter;
            }
        }
        
        public ApplicationPresenter(Application app)
        {
            _app = app;
            _appDb = new AppDbContext("isostore:/AppDb.sdf");
            if (!AppDb.DatabaseExists())
            {
                AppDb.CreateDatabase();
            }
            Repository = new AppDbRepository(AppDb);
            RecentSearchListMaxContent = 10;
            Repository.RecentSearchListMaxContent = RecentSearchListMaxContent;
        }

        public void LoadPresenter()
        {
            RecentSearches = Repository.GetSearchList();
            ReadingList = Repository.GetReadingList();
            SpotLightSearches = Repository.GetSpotLightList();
        }

        public async Task AddToReadingList(PubMedSummary summary)
        {
            if (summary != null)
            {
                Task t = Repository.SetReadingListAsync(summary.PMId, true);
                summary.IsOnReadingList = true;
                if (!ReadingList.Any(s => s.PMId == summary.PMId))
                {
                    ReadingList.Add(summary);
                }
                await t;
            }
        }
        
        public async Task RemoveFromReadingList(PubMedSummary summary)
        {
            if (summary != null)
            {
                Task t = Repository.SetReadingListAsync(summary.PMId, false);
                summary.IsOnReadingList = false;
                while (ReadingList.Any(s => s.PMId == summary.PMId))
                {
                    PubMedSummary itemToRemove = ReadingList.First(s => s.PMId == summary.PMId);
                    ReadingList.Remove(itemToRemove);
                }
                await t;
            }
        }

        public async Task AddToSpotlight(PubMedSearch search)
        {
            if (search != null)
            {
                if (!search.IsBookMarked)
                {
                    Task t = Repository.SetBookmarkSearchAsync(search.Id, true);
                    search.IsBookMarked = true;
                    SpotLightSearches.Add(search);
                    await t;
                }
            }
        }

        public async Task RemoveFromSpotlight(PubMedSearch search)
        {
            if (search !=null)
            {
                if (search.IsBookMarked)
                {
                    Task t = Repository.SetBookmarkSearchAsync(search.Id, false);
                    search.IsBookMarked = false;
                    PubMedSearch itemToRemove = SpotLightSearches.First(s => s.Id == search.Id);
                    SpotLightSearches.Remove(itemToRemove);
                    await t;
                }
            }
        }

        public async Task AddToRecentSearchList(PubMedSearch search)
        {
            if (search !=null)
            {
                if (RecentSearches.Any(s => s.SearchString == search.SearchString))
                {
                    PubMedSearch itemToUpdate = RecentSearches.First(s => s.SearchString == search.SearchString);
                    itemToUpdate = search;
                }
                else
                {
                    RecentSearches.Insert(0, search);
                    while (RecentSearches.Count > RecentSearchListMaxContent)
                    {
                        int i = RecentSearches.Count - 1;
                        RecentSearches.RemoveAt(i);
                    }
                    await Repository.CleanDb();
                }
            }
        }

        public async Task UpdateSpotlightItemName(PubMedSearch spotlightItem, string newName)
        {
            if (spotlightItem.SearchName != newName)
            {
                spotlightItem.SearchName = newName;
                Task t = Repository.UpdateSearchNameAsync(spotlightItem);
                UpdateSeconsaryTileName(spotlightItem);
                await t;
            }
        }

        public void AddSecondaryTile(PubMedSearch searchItem)
        {
            Uri nav = new Uri("/Pages/AdvanceSearchPage.xaml?id=" + searchItem.Id, UriKind.Relative);
            ShellTile exisitingTile = ShellTile.ActiveTiles.FirstOrDefault(t => t.NavigationUri == nav);
            if (exisitingTile == null)
            {
                IconicTileData data = new IconicTileData
                {
                    Count = 0,
                    IconImage = new Uri("/Assets/PubMedTileIcon.png", UriKind.Relative),
                    SmallIconImage = new Uri("/Assets/PubMedSmallTileIcon.png", UriKind.Relative),
                    Title = searchItem.SearchName
                };
                ShellTile.Create(nav, data, true);
            }
        }

        public void UpdateSeconsaryTileName(PubMedSearch searchItem)
        {
            Uri nav = new Uri("/Pages/AdvanceSearchPage.xaml?id=" + searchItem.Id, UriKind.Relative);
            ShellTile exisitingTile = ShellTile.ActiveTiles.FirstOrDefault(t => t.NavigationUri == nav);
            if (exisitingTile != null)
            {
                IconicTileData data = new IconicTileData
                 {
                     Title = searchItem.SearchName
                 };
                exisitingTile.Update(data);
            }
        }

        public void Dispose()
        {
            _appDb.Dispose();
        }
    }
}
