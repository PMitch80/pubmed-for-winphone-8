﻿using PubMedLite.Models;
using PubMedLite.UserControls;
using PubMedLite.BLL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PubMedLite.Presenters
{
    public class AbstractPagePresenter : PresenterBase
    {
        PubMedSearchService _pubMedService;

        public int PMId { get; set; }
        public AbstractPage Page { get { return base._page as AbstractPage; } }

        private bool _isOnReadingList;
        public bool IsOnReadingList { get { return _isOnReadingList; } set { SetProperty(ref this._isOnReadingList, value); /*Tidy*/ Page.AppBar.IsOnReadingList = value; } }

        private PubMedSummary _article;
        public PubMedSummary Article { get { return _article; } set { SetProperty(ref this._article, value); } }

        private ObservableCollection<DbAbstractNote> _notes;
        public ObservableCollection<DbAbstractNote> Notes { get { return _notes; } set { SetProperty(ref this._notes, value); } }

        public Loader LoadingBar { get; set; }

        public AbstractPagePresenter(AbstractPage page)
            : base(page)
        {
            LoadingBar = new Loader();
            _pubMedService = new PubMedSearchService(AppPresenter.Repository, LoadingBar);
        }

        public async void LoadData(int pMId)
        {
            if (pMId != 0)
            {
                PMId = pMId;
                Task<PubMedSummary> getArticle = _pubMedService.GetSummary(pMId);
                Notes = new ObservableCollection<DbAbstractNote>(AppPresenter.Repository.GetNotes(pMId));
                Article = await getArticle;
                if (!Article.IsAbstractAcquired)
                {
                    await _pubMedService.GetAbstractAsync(Article);
                }
            }

        }

        #region NoteFunctions
        public void AddNote(string noteText) 
        {
            DbAbstractNote noteToAdd = new DbAbstractNote()
                {
                    PMId = this.PMId,
                    Note = noteText
                };

            Task t = AppPresenter.Repository.AddNoteAsync(noteToAdd);
            Notes.Add(noteToAdd);
        }

        public void RemoveNote(DbAbstractNote note)
        {
            Task t = AppPresenter.Repository.RemoveNoteAsync(note);
            Notes.Remove(note);
        }
        #endregion

        #region ReadingList Functions

        public async Task AddToReadingList()
        {
            if (!AppPresenter.ReadingList.Any(s => s.PMId == PMId))
            {
                await AppPresenter.AddToReadingList(Article);
                IsOnReadingList = true;
            }
            else
            {
                IsOnReadingList = true;
            }
        }

        public async Task RemoveFromReadingList()
        {
            if (AppPresenter.ReadingList.Any(s => s.PMId == PMId))
            {
                await AppPresenter.RemoveFromReadingList(Article);
                IsOnReadingList = false;
            }
            else
            {
                IsOnReadingList = false;
            }
        }

        #endregion


        
    }
}
