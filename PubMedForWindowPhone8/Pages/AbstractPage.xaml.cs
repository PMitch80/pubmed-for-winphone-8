﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PubMedLite.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using PubMedLite.UserControls;
using PubMedLite.Presenters;
using Microsoft.Phone.Tasks;
using PubMedLite.BLL;

namespace PubMedLite
{
    public partial class AbstractPage : PhoneApplicationPage
    {
        
       
        public AbstractPagePresenter Presenter;

        public AbstractPageAppBar AppBar;

        public int _tombstoneState;
        bool _isNewPageInstance = false;
        
        public AbstractPage()
        {
            InitializeComponent();

            Presenter = new AbstractPagePresenter(this);
            
            DataContext = Presenter;

            AppBar = new AbstractPageAppBar(Presenter);
           
            ApplicationBar = AppBar.ApplicationBar;

            _isNewPageInstance = true;
        }

        #region Navigation

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string pMId = "";
            if (NavigationContext.QueryString.TryGetValue("id", out pMId))
            {
                
                Presenter.LoadData(Convert.ToInt32(pMId));
            }
            if (_isNewPageInstance && Presenter != null && State.Count > 0)
            {
                _tombstoneState = (int)State["AbstractPageTombstoneState"];
                PanoramaRoot.DefaultItem = PanoramaRoot.Items[_tombstoneState];
            }
            _isNewPageInstance = false;
        }

        protected async override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            Task t = null;
            if (Presenter.Notes.Count > 0)
            {
                if (!Presenter.IsOnReadingList)
                {
                    t = Presenter.AddToReadingList();
                }
            }
            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {

                _tombstoneState = PanoramaRoot.SelectedIndex;
                State["AbstractPageTombstoneState"] = _tombstoneState;
            }
            base.OnNavigatingFrom(e);
            if (t != null)
            {
                await t;
            }
        }

        #endregion

        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            Presenter.IsOnReadingList = Presenter.AppPresenter.ReadingList.Any(s => s.PMId == Presenter.PMId);
        }

        
        #region NoteFunctions
        
        private void NoteInput_ReturnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Presenter.AddNote(NoteInput.Text);
                NoteInput.Text = "";
                NoteBox.Focus();
            }
        }

        private void DeleteNote_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Image icon = sender as Image;
            if (icon != null)
            {
                DbAbstractNote note = icon.DataContext as DbAbstractNote;
                if (note != null)
                {
                    Presenter.RemoveNote(note);
                }
            }
        }
       
        #endregion

        private void Textbox_GotFocus(object sender, RoutedEventArgs e)
        {
            AppBar.Hide();
        }

        private void Textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            AppBar.Show();
        }

        

        

        


    }
}