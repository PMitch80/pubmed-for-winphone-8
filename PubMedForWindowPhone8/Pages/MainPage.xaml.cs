﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PubMedLite.Resources;
using PubMedLite.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Threading.Tasks;
using PubMedLite.UserControls;
using PubMedLite.Presenters;
using System.Windows.Media;
using System.Windows.Shapes;
using PubMedLite.BLL;
using System.Windows.Controls.Primitives;

namespace PubMedLite
{
    public partial class MainPage : PhoneApplicationPage
    {
        private bool _isSearching; 
        
        public MainPagePresenter Presenter;

        public MainPageAppBar AppBar;

        public enum MainPageSubpage { Search = 0, Recent = 1, SpotLight = 2, ReadingList = 3 }
        
        public MainPageTombstoneState _tombstoneState;
        bool _isNewPageInstance = false;
        
        public MainPage()
        {
            InitializeComponent();
            Presenter = new MainPagePresenter(this);
            DataContext = Presenter;
            AppBar = new MainPageAppBar(Presenter);
            ApplicationBar = AppBar.ApplicationBar;
            SpotlightDialog.OnTextInput += NewSpotlightText;
            _isNewPageInstance = true;
            
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                int id = 0;
                if (Presenter.OpenSearch != null)
                {
                  id = Presenter.OpenSearch.Id;
                }

                _tombstoneState = new MainPageTombstoneState()
                {
                    SelectedPageIndex = (int)Presenter.SelectedPage,
                    OpenSearchId = id
                };
                State["MainPageTombstoneState"] = _tombstoneState;
            }
        }

        protected async override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (_isNewPageInstance && Presenter != null && State.Count > 0)
            {
                _tombstoneState = (MainPageTombstoneState)State["MainPageTombstoneState"];
                LayoutRoot.SelectedIndex = _tombstoneState.SelectedPageIndex;
                await Presenter.LoadSearchResult(_tombstoneState.OpenSearchId);
            }
            _isNewPageInstance = false;
        }


        private async void NewSpotlightText(object sender, EventArgs e)
        {
            string text = sender as string;
            if (text != null)
            {
                PubMedSearch spotlightItem = SpotlightList.SelectedItem as PubMedSearch;
                if (spotlightItem != null)
                {
                    await Presenter.AppPresenter.UpdateSpotlightItemName(spotlightItem, text);
                }
            }
        }

        private async void SearchQueryBox_EnterKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!_isSearching)
                {
                    _isSearching = true;
                    LayoutRoot.Focus();
                    if (!string.IsNullOrWhiteSpace(SearchQueryBox.Text))
                    {
                        await Presenter.Search(SearchQueryBox.Text);
                        _isSearching = false;
                    }
                }
            }
        }


        private async void PageTurned(object sender, SelectionChangedEventArgs e)
        {
                await Presenter.PageTurned();
        }

        private void SearchItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            StackPanel panel = sender as StackPanel;
            if (panel != null)
            {
                PubMedSummary item = panel.DataContext as PubMedSummary;
                if (item.PMId != 0)
                {
                    NavigationService.Navigate(new Uri("/Pages/AbstractPage.xaml?id=" + item.PMId, UriKind.Relative));
                }
            }
        }

        private async void ReadingList_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                PubMedSummary item = element.DataContext as PubMedSummary;
                if (item != null)
                {
                    if (item.PMId != 0)
                    {
                        if (item.IsOnReadingList)
                        {
                            await Presenter.AppPresenter.RemoveFromReadingList(item);
                        }
                        else
                        {
                            await Presenter.AppPresenter.AddToReadingList(item);
                        }
                    }
                }
            }
        }

        public void PopUp()
        {
            PubMedSearch spotlightItem = SpotlightList.SelectedItem as PubMedSearch;
            if (spotlightItem != null)
            {
                SpotlightDialog.Show(spotlightItem.SearchName);
            }
        }

        private void AdvanceSearch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                PubMedSearch selectedItem = element.DataContext as PubMedSearch;
                if (selectedItem != null)
                {
                    NavigationService.Navigate(new Uri("/Pages/AdvanceSearchPage.xaml?id=" + selectedItem.Id, UriKind.Relative));
                }
            }
        }

        private void Selected_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                PubMedSummary item = element.DataContext as PubMedSummary;
                if (item != null)
                {
                    if (item.IsSelected)
                    {
                        item.IsSelected = false;
                    }
                    else
                    {
                        item.IsSelected = true;
                    }

                    if(Presenter.ReadingList.Any(a =>a.IsSelected))
                    {
                        AppBar.Show();
                    }
                    else
                    {
                        AppBar.Hide();
                    }
                }
            }
        }

        private void SelectedSearch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                Presenter.ClearSearchSelections();
                PubMedSearch item = element.DataContext as PubMedSearch;

                if (item != null)
                {
                    item.IsSelected = true;
                }
            }
        }

        private void OnOrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            if ((e.Orientation & PageOrientation.Portrait) == (PageOrientation.Portrait))
            {
                Container.Margin = new Thickness(0, 0, 0, 0);
            }
            else if ((e.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            {
                Container.Margin = new Thickness(0, 0, 48, 0);
            }
            else if ((e.Orientation & PageOrientation.LandscapeRight) == (PageOrientation.LandscapeRight))
            {
                Container.Margin = new Thickness(48, 0, 0, 0);
            } 
        }
        
        public void ClearSelectedSearchs()
        {
            RecentSearchesList.SelectedItem = null;
            SpotlightList.SelectedItem = null;
        }

       


    }
}