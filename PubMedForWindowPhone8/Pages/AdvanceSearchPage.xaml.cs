﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PubMedLite.Presenters;
using PubMedLite.Models;
using System.Windows.Input;
using PubMedLite.UserControls;
using System.Windows.Shapes;
using PubMedLite.BLL;

namespace PubMedLite
{
    public partial class AdvanceSearchPage : PhoneApplicationPage
    {
        public AdvanceSearchPagePresenter Presenter;

        public AdvanceSearchPageAppBar AppBar;

        public enum AdvanceSearchSubpage { Search = -1, Year = 0, FirstAuthor = 1, LastAuthor = 2, Journal = 3 }

        public AdvanceSearchSubpage _currentPage { get { return (AdvanceSearchSubpage)LayoutRoot.SelectedIndex; } }

        public int NavigatedToId;

        //tombstoning
        public int _tombstoneState;
        bool _isNewPageInstance = false;

        public AdvanceSearchPage()
        {
            InitializeComponent();
            Presenter = new AdvanceSearchPagePresenter(this);
            DataContext = Presenter;

            AppBar = new AdvanceSearchPageAppBar(Presenter);
            ApplicationBar = AppBar.ApplicationBar;
            _isNewPageInstance = true;
        }

        #region Navigation

        protected async override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string searchId = "";
            if (NavigationContext.QueryString.TryGetValue("id", out searchId))
            {
                NavigatedToId = Convert.ToInt32(searchId);
            }

            if (e.NavigationMode != NavigationMode.Back)
            {
                await Presenter.LoadResult(NavigatedToId);
            }
            else
            {
                if (_isNewPageInstance && Presenter != null && State.Count > 0)
                {
                    _tombstoneState = (int)State["AdvanceSearchPageTombstoneState"];
                    LayoutRoot.SelectedIndex = _tombstoneState;
                    await Presenter.LoadResult(NavigatedToId);
                }
                
            }
            _isNewPageInstance = false;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {

                _tombstoneState = LayoutRoot.SelectedIndex;
                State["AdvanceSearchPageTombstoneState"] = _tombstoneState;
            }
            base.OnNavigatingFrom(e);
        }
        #endregion


        private async void SearchQueryBox_EnterKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Presenter.SearchQuery = SearchQueryBox.Text;
                await Presenter.Search();
                LayoutRoot.Focus();
            }
        }

        private void SelectedPageChanged(object sender, SelectionChangedEventArgs e)
        {
                Presenter.PageTurned();
        }

        private async void ReadingList_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element != null)
            {
                PubMedSummary item = element.DataContext as PubMedSummary;
                if (item != null)
                {
                    if (item.IsOnReadingList)
                    {
                        await Presenter.AppPresenter.RemoveFromReadingList(item);
                    }
                    else
                    {
                        await Presenter.AppPresenter.AddToReadingList(item);
                    }

                }
            }
        }

        private void SummaryItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PubMedSummary summary;
            StackPanel item = sender as StackPanel;
            if (item != null)
            {
                summary = item.DataContext as PubMedSummary;

            }
            else
            {
                TextBlock textItem = sender as TextBlock;
                if (textItem != null)
                {
                    summary = textItem.DataContext as PubMedSummary;
                }
                else
                {
                    return;
                }
            }
            if (summary != null)
            {
                NavigationService.Navigate(new Uri("/Pages/AbstractPage.xaml?id=" + summary.PMId, UriKind.Relative));
            }
        }

        private void OnOrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            if ((e.Orientation & PageOrientation.Portrait) == (PageOrientation.Portrait))
            {
                LayoutRoot.Margin = new Thickness(0, 0, 0, 0);
            }
            else if ((e.Orientation & PageOrientation.LandscapeLeft) == (PageOrientation.LandscapeLeft))
            {
                LayoutRoot.Margin = new Thickness(0, 0, 48, 0);
            }
            else if ((e.Orientation & PageOrientation.LandscapeRight) == (PageOrientation.LandscapeRight))
            {
                LayoutRoot.Margin = new Thickness(48, 0, 0, 0);
            }
        }
    }
}