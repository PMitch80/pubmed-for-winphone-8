﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public class Loader : UIThreadNotifier
    {
        private bool _isLoading;

        public bool IsLoading
        {
            get { return _isLoading; }
            set 
            { 
                this.SetProperty(ref this._isLoading, value);
                SetLoading(value);
            }
        }

        private string _loadingBarMessage;

        public string LoadingBarMessage
        {
            get { return _loadingBarMessage; }
            set { this.SetProperty(ref this._loadingBarMessage, value); }
        }

        public Loader()
        {
            _loadingBarMessage = "Loading...";
            _isLoading = false;
        }

        protected virtual void SetLoading(bool value)
        {
            if (value == true)
            {
                LoadingBarMessage = "Loading...";
            }
        }

        
    }
}
