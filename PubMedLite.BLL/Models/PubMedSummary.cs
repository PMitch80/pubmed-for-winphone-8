﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    
    public class PubMedSummary : UIThreadNotifier
    {
        public int PMId {get; set;}
        public string PMCId { get; set; }
        public List<string> AuthorList {get; set;}
        public string AuthorSummary { get; set; }
        public DateTime? PubDate {get; set;}
        public string Source {get; set;}
        public string SourceRef { get; set; }
        public string Title {get; set;}

        private string _firstAuthor;
        public string FirstAuthor 
        { 
            get 
            {
                _firstAuthor = AuthorList.FirstOrDefault();
                if (_firstAuthor == null)
                {
                    _firstAuthor = "";
                }
                return _firstAuthor; 
            } 
        }

        private string _lastAuthor;
        public string LastAuthor 
        { 
            get 
            {
                
                _lastAuthor = AuthorList.LastOrDefault();
                if (_lastAuthor == null)
                {
                    _lastAuthor = "";
                }
                return _lastAuthor;
            } 
        }
        public bool IsAbstractAcquired { get; set; }
        public bool IsSummaryAcquired { get; set; }

        private string _abstract;
        public string Abstract
        {
            get { return _abstract; }
            set { SetProperty(ref this._abstract, value); }
        }

        private bool _isOnReadingList;
        public bool IsOnReadingList 
        {
            get { return _isOnReadingList; }
            set { SetProperty(ref this._isOnReadingList, value); } 
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public PubMedSummary()
        {
            AuthorList = new List<string>();
        }
    }
}
