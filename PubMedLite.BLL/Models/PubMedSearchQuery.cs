﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public class PubMedSearchQuery
    {
        public PubMedSearch Search { get; set; }
        public List<PubMedSummary> Result { get; set; }

        public PubMedSearchQuery()
        {
            Result = new List<PubMedSummary>();
        }
    }
}
