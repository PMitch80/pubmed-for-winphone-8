﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    
    public class PubMedSearch : Notifier
    {
       
        public int Id { get; set; }

        private string _searchName;
       
        public string SearchName { get { return _searchName; } set { this.SetProperty(ref this._searchName, value); } }
      
        public string SearchString { get; set; }
       
        public DateTime DateRequested { get; set; }
       
        public int Query { get; set; }
       
        public int Count { get; set; }
       
        public int NewItems { get; set; }
      
        public string WebEnviroment { get; set; }
       
        public List<int> PIdList { get; set; }
       
        public bool IsBookMarked { get; set; }

        bool _isSelected;
        
        public bool IsSelected 
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public PubMedSearch()
        {
            PIdList = new List<int>();
        }
    }
}
