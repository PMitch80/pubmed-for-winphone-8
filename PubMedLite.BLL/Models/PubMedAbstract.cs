﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public class PubMedAbstract
    {
        public PubMedSummary Summary { get; set; }
        public string Abstract { get; set; }
        public string PMCId { get; set; }

        public PubMedAbstract()
        {
            Summary = new PubMedSummary();
        }
    }
}
