﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    /// <summary>
    /// Organises the Search and Retrieval of pubmed articles, using the Pubmed API and Local database
    /// </summary>
    public class PubMedSearchService
    {
        private PubMedRepository _pubMedRepository;
        private IAppDbRepository _appDbRespository;
        
        private Loader _loader;
        private int _shortSearchLimit;
        PubMedSummary _emptySearchSummary;
       
        /// <summary>
        /// Event on Additional Pubmed Summaries Found
        /// Sender is The Additional Summaries
        /// </summary>
        public event EventHandler NewSummariesAdded;

        public PubMedSearchService(IAppDbRepository appDbRespository, Loader loader = null)
        {
            _pubMedRepository = new PubMedRepository();
            _appDbRespository = appDbRespository;
            _loader = loader;
            _shortSearchLimit = 50;
            _emptySearchSummary = new PubMedSummary { PMId = 0, Title = "No results found." };
        }

        #region Public Async Methods

        public Task<PubMedSearchQuery> SearchPubmedAsync(string searchQuery)
        {
            return Task<PubMedSearchQuery>.Run(() => SearchPubmed(searchQuery));
        }

        public Task<PubMedSearchQuery> GetSearchResultsAsync(int searchId)
        {
            return Task<PubMedSearchQuery>.Run(() => GetSearchResults(searchId));
        }

        public Task GetAdditionalResultListAsync(PubMedSearch search)
        {
            return Task.Run(() => GetAdditionalResultList(search));
        }

        public Task GetAbstractAsync(PubMedSummary result)
        {
            return Task.Run(() => GetAbstract(result));
        }
        #endregion

        #region Search Methods

        /// <summary>
        /// Searchs PubMed by Search Query Text and retrieves Pubmed summaries
        /// </summary>
        /// <param name="searchQuery">Search Query Text</param>
        /// <returns>Observable Collection of PubMed Summaries for search query</returns>
        private async Task<PubMedSearchQuery> SearchPubmed(string searchQuery)
        {
            if (string.IsNullOrWhiteSpace(searchQuery))
            {
                return null;
            }
            PubMedSearchQuery result = new PubMedSearchQuery();
            SetLoader(true, "Connecting To PubMed");
            bool waitForLoader = false;
            result.Search = await _pubMedRepository.Search(searchQuery);
            if (result.Search != null)
            {
                await _appDbRespository.AddSearchAsync(result.Search);
                if (result.Search.PIdList.Count != 0)
                {
                    result.Result = await GetSummaries(result.Search, 1, _shortSearchLimit);
                }
                else
                {
                    result.Result = new List<PubMedSummary>();
                    result.Result.Add(_emptySearchSummary);
                }
            }
            if(result.Result == null)
            {
                SetLoader("Connection Failed");
                waitForLoader = true;
            }
            if (waitForLoader)
            {
                await Task.Delay(1000);
            }
            SetLoader(false);
            return result;
        }

        /// <summary>
        /// Gets Previous Search result
        /// </summary>
        /// <param name="searchId">Search Id</param>
        /// <returns>Observable Collection of PubMed Summaries for Search Id</returns>
        private async Task<PubMedSearchQuery> GetSearchResults(int searchId)
        {
            
            PubMedSearchQuery result = new PubMedSearchQuery();
            SetLoader(true, "Acquiring search from memory");
            result.Search = await _appDbRespository.GetSearchAsync(searchId);
            if (result.Search.DateRequested > DateTime.UtcNow.AddHours(1))
            {
                SetLoader(true, "Looking for new Results");
                int count = await _pubMedRepository.GetNumberOfNewResults(result.Search);
                if (count > 0)
                {
                    
                    result = await SearchPubmed(result.Search.SearchString);
                    if (result.Result != null)
                    {
                        return result;
                    }
                   
                }
                else
                {
                    result.Result = new List<PubMedSummary>();
                    result.Result.Add(_emptySearchSummary);
                }
            }
            if (result.Search.Count!= 0)
            {
                result.Result = await GetSummaries(result.Search, 1, _shortSearchLimit);
            }
            else
            {
                result.Result = new List<PubMedSummary>();
                result.Result.Add(_emptySearchSummary);
            }
            SetLoader(false);
            return result;
        }

        /// <summary>
        /// Aquires Summaries from a Search Result
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        private async Task GetAdditionalResultList(PubMedSearch search)
        {
            SetLoader(true, "Gathering Additional Results");
            int start = _shortSearchLimit;
            int maxcount = search.PIdList.Count;
            while (maxcount > start)
            {
                int availableRange = 200;
                if (maxcount - start < availableRange)
                {
                    availableRange = maxcount - start;
                }
                
                List<PubMedSummary> summariesToLoad = await GetSummaries(search, start + 1, start + availableRange);

                UIThread.Invoke(() => OnAdditionalResults(summariesToLoad));
                start += availableRange;
            }
            SetLoader(false);
            await _appDbRespository.CleanArticleListAsync();
        }

        #endregion

        #region Acquire Article Methods

        /// <summary>
        /// Aquires Summaries from the select Search result Range
        /// </summary>
        /// <param name="search"> Search Query</param>
        /// <param name="start">Starting Point in Query list</param>
        /// <param name="end">End Point</param>
        /// <returns></returns>
        private async Task<List<PubMedSummary>> GetSummaries(PubMedSearch search, int start, int end)
        {
            bool waitForLoader = false;

            if (end > search.PIdList.Count)
            {
                end = search.PIdList.Count;
            }
            List<int> searchList = (from l in search.PIdList.Skip(start - 1).Take(end - start + 1) select l).ToList();

            string s = search.Count + " results. Loading results " + start + "-" + end;
            SetLoader(s);

            List<PubMedSummary> result = await _appDbRespository.GetAvailableSummariesAsync(searchList);
            List<int> summariesToGet = result.Where(m => m.IsSummaryAcquired == false).Select(m => m.PMId).ToList();
            if (summariesToGet.Count != 0)
            {
                List<PubMedSummary> listToMerge = await _pubMedRepository.GetPubMedSummaries(summariesToGet);
                
                if (listToMerge != null)
                {
                    Task t = _appDbRespository.AddSummaryAsync(listToMerge);
                    foreach (PubMedSummary item in listToMerge)
                    {
                        PubMedSummary slotToFill = result.FirstOrDefault(q => q.PMId == item.PMId);
                            if (slotToFill != null)
                            {
                                int i = result.IndexOf(slotToFill);
                                result[i] = item;
                            }
                    } 
                    await t;
                }
                else
                {
                    SetLoader("Connection Failed");
                    waitForLoader = true;
                }
                listToMerge.RemoveAll(m => !m.IsSummaryAcquired);
                if (waitForLoader)
                {
                    await Task.Delay(1000);
                }
            }
            return result;

        }

        
        /// <summary>
        /// Return Abstract From pubmed Id
        /// *Need to Improve Async method *
        /// </summary>
        /// <param name="id">PubMed Id</param>
        /// <returns>Pubmed Abstact for PubMed Id</returns>
        private async Task GetAbstract(PubMedSummary result)
        {
            SetLoader(true, "Getting Abstract"); 
            await _pubMedRepository.GetAbstract(result);
            if (result.IsAbstractAcquired)
            {
                await _appDbRespository.AddAbstractAsync(result);
            }
            SetLoader(false);
        }

        public Task<PubMedSummary> GetSummaryAsync(int id)
        {
            return Task<PubMedSummary>.Run(()=>GetSummary(id));
        }

        public async Task<PubMedSummary> GetSummary(int id)
        {
            PubMedSummary summary = await _appDbRespository.GetSummaryAsync(id);
            if (summary == null)
            {
                List<int> ids = new List<int>();
                ids.Add(id);
                summary = (await _pubMedRepository.GetPubMedSummaries(ids)).FirstOrDefault();
            }
            return summary;
        }


        #endregion

        #region Preview Methods

        public async Task<int> GetNewResultTotal()
        {
            return await _appDbRespository.GetTotalNewResultInSearchAsync();
        }

        public async Task<PubMedSearch> UpdateNextSearch()
        {
            PubMedSearch search = await _appDbRespository.GetNextSearchForCountUpdateAsync();
            if (search != null)
            {
                int newItems = await _pubMedRepository.GetNumberOfNewResults(search);
                await _appDbRespository.UpdateSearchCountAsync(search.Id, newItems);
            }
            return search;
        }

        #endregion

        #region ServiceMethods

        private void SetLoader(bool isLoading, string message = null)
        {
            
            if (_loader != null)
            {
                _loader.IsLoading = isLoading;
                if (message != null)
                {
                    _loader.LoadingBarMessage = message;
                }
            }
               
        }

        private void SetLoader(string message)
        {
            if (_loader != null)
            {
                _loader.LoadingBarMessage = message;
            }
        }

        private void OnAdditionalResults(object newResults, EventArgs arg = null)
        {
            if (NewSummariesAdded != null)
                NewSummariesAdded(newResults, arg);
        }
        #endregion
    }
}
