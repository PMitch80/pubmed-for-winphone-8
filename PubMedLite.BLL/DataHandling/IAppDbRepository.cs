﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public interface IAppDbRepository
    {
        #region searchs

       Task AddSearchAsync(PubMedSearch search);

       void AddSearch(PubMedSearch search);

       Task<ObservableCollection<PubMedSearch>> GetSearchListAsync();

       ObservableCollection<PubMedSearch> GetSearchList();

       Task<PubMedSearch> GetSearchAsync(int searchId);

       PubMedSearch GetSearch(int searchId);

        #endregion

        #region Spotlight

       Task<ObservableCollection<PubMedSearch>> GetSpotLightListAsync();

       ObservableCollection<PubMedSearch> GetSpotLightList();

       Task SetBookmarkSearchAsync(int id, bool isBookmarked);
      
       void SetBookmarkSearch(int id, bool isBookmarked);

       Task UpdateSearchCountAsync(int searchId, int count);

       void UpdateSearchCount(int searchId, int count);

       Task<PubMedSearch> GetNextSearchForCountUpdateAsync();

       PubMedSearch GetNextSearchForCountUpdate();

       Task<int> GetTotalNewResultInSearchAsync();

       int GetTotalNewResultInSearch();

       Task UpdateSearchNameAsync(PubMedSearch spotlightItem);

       void UpdateSearchName(PubMedSearch spotlightItem);

        #endregion

        #region Summaries

        Task<List<PubMedSummary>> GetAvailableSummariesAsync(List<int> searchList);

        List<PubMedSummary> GetAvailableSummaries(List<int> searchList);

        Task<PubMedSummary> GetSummaryAsync(int i);

        PubMedSummary GetSummary(int i);

        Task AddSummaryAsync(IEnumerable<PubMedSummary> summary);

        void AddSummary(IEnumerable<PubMedSummary> summary);

        #endregion

        #region Abstracts

       PubMedAbstract GetAbstract(int id);

       Task AddAbstractAsync(PubMedSummary result);

       void AddAbstract(PubMedSummary result);
        
        #endregion

        #region Notes

       List<DbAbstractNote> GetNotes(int id);

       Task AddNoteAsync(DbAbstractNote note);

       void AddNote(DbAbstractNote note);

       Task RemoveNoteAsync(DbAbstractNote note);

       void RemoveNote(DbAbstractNote note);
       
        #endregion

        #region Reading List

        ObservableCollection<PubMedSummary> GetReadingList();

        Task SetReadingListAsync(int pMId, bool isOnReadinList);

        void SetReadingList(int pMId, bool isOnReadinList);

        #endregion

        #region Disposal Methods

        Task CleanDb();

        Task CleanSearchListAsync();

        Task CleanArticleListAsync();

        void CleanArticleList();
        
        #endregion
    }
}
