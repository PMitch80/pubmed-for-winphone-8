﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PubMedLite.BLL
{
    class PubMedRepository
    {
        public int SearchReturnMax { get; set; }
        public int AdvanceSearchReturnMax { get; set; }

        public PubMedRepository()
        {
            SearchReturnMax = 1000;
            AdvanceSearchReturnMax = 1000;
        }

        public async Task<PubMedSearch> Search(string searchQuery)
        {
            if (String.IsNullOrWhiteSpace(searchQuery))
            {
                return null;
            }
            
            PubMedSearch result = new PubMedSearch();
            result.SearchString = searchQuery;
            result.SearchName = searchQuery;
            HttpClient client = new HttpClient();
            
            string url = SearchURLStringBuilder(searchQuery);
            
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var x = await response.Content.ReadAsStreamAsync();
                XElement doc = XElement.Load(x);
                result.Count = Convert.ToInt32(doc.Element("Count").Value);
                result.DateRequested = DateTime.UtcNow;
                result.PIdList = GetIdList(doc.Element("IdList").Elements("Id"));
                result.WebEnviroment = doc.Element("WebEnv").Value;
                result.Query = Convert.ToInt32(doc.Element("QueryKey").Value);
                return result;
            }

            return null;
        }

        public async Task<List<PubMedSummary>> GetPubMedSummaries(List<int> summariesToGet)
        {
            List<PubMedSummary> results = new List<PubMedSummary>();
            HttpClient client = new HttpClient();
            string url = SummaryURLStringBuilder(summariesToGet);
            
            
            HttpResponseMessage response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var x = await response.Content.ReadAsStreamAsync();
                XElement doc = XElement.Load(x);
                List<XElement> XSummary = (
                    from el in doc.Descendants("DocSum") select el).ToList();
                List<Task> ts = new List<Task>();
                foreach (XElement docSum in XSummary)
                {
                    ts.Add(Task.Run(() => results.Add(GetSummary(docSum))));
                }
                Task.WaitAll(ts.ToArray());

                return results;
            }
            else
            {
                return null;
            }
        }

        private Task<PubMedSummary> GetSummaryAsync(XElement summary)
        {
            
            return Task<PubMedSummary>.Run(() => { return GetSummary(summary); });
        }
        
        private PubMedSummary GetSummary(XElement summary)
        {
            PubMedSummary result = new PubMedSummary();
            result.PMId = Convert.ToInt32(summary.Element("Id").Value);
            
            string s = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "PubDate" select el.Value).FirstOrDefault();
            DateTime value = new DateTime();
            bool hasData =  DateTime.TryParse(s, out value);
            if (hasData)
            {
                result.PubDate = value;
            }
            else
            {
                string s2 = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "EPubDate" select el.Value).FirstOrDefault();
                hasData = DateTime.TryParse(s2, out value);
                if (hasData)
                {
                    result.PubDate = value;
                }
                else
                {
                    result.PubDate = null;
                }
            }
            
            result.Source = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Source" select el.Value).FirstOrDefault();
            result.SourceRef = BuildSourceRef(summary);
            result.Title = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Title" select el.Value).FirstOrDefault();
            result.AuthorList = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Author" select el.Value).ToList();
            result.IsAbstractAcquired = false;
            result.IsSummaryAcquired = true;

            if (result.AuthorList.Count > 3)
            {
                int i = result.AuthorList.Count - 1;
                string s3 = result.AuthorList[0] + ", " + result.AuthorList[1] + "... " + result.AuthorList[i];
                result.AuthorSummary = s3;
            }
            else
            {
                string s4 = "";
                string comma = ", ";
                foreach (string item in result.AuthorList)
                {
                    s4 += item + comma;
                }
                s4 = s4.TrimEnd(comma.ToCharArray());
                result.AuthorSummary = s4;
            }

            return result;
        }

        private string BuildSourceRef(XElement summary)
        {
            string vol = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Volume" select el.Value).FirstOrDefault();
            string issue = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Issue" select el.Value).FirstOrDefault();
            string page = (from el in summary.Descendants("Item") where (string)el.Attribute("Name") == "Pages" select el.Value).FirstOrDefault();
            if (string.IsNullOrEmpty(page))
            {
                return null;
            }
            else
            {
                if(!string.IsNullOrEmpty(vol))
                {
                    string result = vol;
                    if (!string.IsNullOrEmpty(issue))
                    {
                        result += "(" + issue + ")";
                    }
                    result += ":" + page;
                    return result;
                }
                else if (!string.IsNullOrEmpty(issue))
                {
                    string result = "(" + issue + "):" + page;
                    return result;
                }
                else
                {
                    return page;
                }
            }

        }

        private DateTime GetDate(XElement DateNode)
        {
            int year = Convert.ToInt32(DateNode.Element("Year").Value);
            int month = Convert.ToInt32(DateNode.Element("Month").Value);
            int day = Convert.ToInt32(DateNode.Element("Day").Value);
            return new DateTime(year, month, day);
        }

        private List<string> GetAuthors(IEnumerable<XElement> authorListNode)
        {
            List<string> results = new List<string>();
            foreach (XElement item in authorListNode)
            {
                string s = item.Element("LastName").Value + " " + item.Element("Initials").Value;
                results.Add(s);
            }
            return results;
        }

        private List<int> GetIdList(IEnumerable<XElement> idList)
        {
            List<int> results = new List<int>();
            foreach (XElement item in idList)
            {
                results.Add(Convert.ToInt32(item.Value));
            }
            return results;
        }

        public async Task GetAbstract(PubMedSummary result)
        {
            string queryUrl = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=" + result.PMId + "&retmode=xml";

            
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(queryUrl);
            if (response.IsSuccessStatusCode)
            {
                var x = await response.Content.ReadAsStreamAsync();
                XElement doc = XElement.Load(x);
                XElement medlineCitation = doc.Element("PubmedArticle").Element("MedlineCitation");
                XElement abstractEl = medlineCitation.Element("Article").Element("Abstract");
                if (abstractEl != null)
                {
                    result.Abstract = abstractEl.Element("AbstractText").Value;
                }
                else
                {
                    result.Abstract = "No Abstract Available";
                }
                XElement articleList = doc.Element("PubmedArticle").Element("PubmedData").Element("ArticleIdList");
                if (articleList != null)
                {
                    result.PMCId = (from el in articleList.Descendants("ArticleId") where (string)el.Attribute("IdType") == "pmc" select el.Value).FirstOrDefault();
                }

                result.IsAbstractAcquired = true;
                
            }
            else
            {
                result.Abstract = "Cound not connect";
            }

        }

        public async Task<int> GetNumberOfNewResults(PubMedSearch search)
        {
            string s = CurrentResultCountURLStringBuilder(search);
            int i = -1;
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(s);
                var x = await response.Content.ReadAsStreamAsync();
                XElement doc = XElement.Load(x);
                int newCount = Convert.ToInt32(doc.Element("Count").Value);
                i = newCount - search.Count;
                search.NewItems = i;
            }
            catch
            {
                i = -1;
            }
            return i;
        }

        #region QueryString Builders

        /// <summary>
        /// Builds Get Summary Url from Pubmed Id
        /// </summary>
        /// <param name="id">PubMed Id</param>
        /// <returns>Url to aquire Summary from PubMed</returns>
        private string SummaryURLStringBuilder(int id)
        {
            List<int> items = new List<int>();
            items.Add(id);
            return SummaryURLStringBuilder(items);
        }

        /// <summary>
        /// Builds Get Summary Url from list of Pubmed Id
        /// </summary>
        /// <param name="listOfIds">List of PubMed Id</param>
        /// <returns>Url to aquire Summaries from PubMed<</returns>
        private string SummaryURLStringBuilder(List<int> listOfIds)
        {
            if (listOfIds.Count >= 200)
            {
                int i = listOfIds.Count - 199;
                listOfIds.RemoveRange(199, i);
            }
            string s = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=";

            foreach (int x in listOfIds)
            {
                s += x + ",";
            }
            s = s.TrimEnd(',');
            return s;
        }

        /// <summary>
        /// Builds Url from Search Query text
        /// </summary>
        /// <param name="searchQuery">Search Query Text</param>
        /// <returns>Url to aquire search result from pubmed</returns>
        private string SearchURLStringBuilder(string searchQuery)
        {
            string s = BuildQuery(searchQuery);
            s = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=" + s + "&usehistory=y&retmax=" + SearchReturnMax;
            return s;
        }

        private string CurrentResultCountURLStringBuilder(PubMedSearch search)
        {
            string s = BuildQuery(search.SearchString);
            return "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=" + s +"&rettype=count";
        }


        private string BuildQuery(string searchInput)
        {
            string result = "";
            if (searchInput != null)
            {
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                result = regex.Replace(searchInput, @"+");
                result = HttpUtility.HtmlEncode(result);
            }
            return result;
        }



        #endregion
    }
}
