﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL

{
    public static class PubMedSummaryMapperExtensions
    {
        public static List<PubMedSummary> BuildIndexedList(this List<PubMedSummary> list, List<int> index)
        {
            if (list.Count == 0)
            {
                foreach (int item in index)
                {
                    list.Add(new PubMedSummary { PMId = item, IsSummaryAcquired = false });
                }
                return list;
            }
            else
            {
                //aternative for completed list
                return list;
            }
        }

        public static List<PubMedSummary> AddToIndexedList(this List<PubMedSummary> list, PubMedSummary summaryToAdd)
        {
            if (summaryToAdd != null)
            {
                
                    PubMedSummary item = list.FirstOrDefault(s => s.PMId == summaryToAdd.PMId);
                    if (item != null)
                    {
                        int i = list.IndexOf(item);
                        list[i] = summaryToAdd;
                    }
            }
            return list;
        }

        public static PubMedSummary MapFromDataBase(this PubMedSummary summary, DbPubmedArticle source)
        {
            summary.PMId = source.PMId;
            summary.PubDate = source.PubDate;
            summary.Abstract = source.Abstract;
            summary.AuthorSummary = source.AuthorSummary;
            summary.Source = source.Source;
            summary.SourceRef = source.SourceRef;
            summary.Title = source.Title;
            summary.IsAbstractAcquired = source.AbstractAquired;
            summary.IsSummaryAcquired = true;
            summary.IsOnReadingList = source.IsOnReadingList;
            return summary;
        }
    }
}
