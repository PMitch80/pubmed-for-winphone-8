﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    class QueryStringBuilder
    {
        public static string BuildQuery(string searchInput)
        {
            Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
            string result = regex.Replace(searchInput, @"+");
            result = HttpUtility.HtmlEncode(result);
            return result;
        }
    }
}
