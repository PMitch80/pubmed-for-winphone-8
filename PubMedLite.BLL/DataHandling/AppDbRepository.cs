﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public class AppDbRepository : IAppDbRepository
    {
        private AppDbContext _db;
        private int _searchListLimit;
        public int RecentSearchListMaxContent 
        {
            get { return _searchListLimit; } 
            set
            {
                if (value < 1)
                {
                    _searchListLimit = 1;
                }
                else
                {
                    _searchListLimit = value;
                }
            } 
        }
        public int MaximunNumberOfArticles { get; set; }
        public static AutoResetEvent OperationOnDatabase = new AutoResetEvent(true);

        public AppDbRepository(AppDbContext db)
        {
            _db = db;
            _searchListLimit = 10;
            if (_searchListLimit < 1)
            {
                _searchListLimit = 1;
            }
            MaximunNumberOfArticles = 10000;
        }

        #region searchs

        public Task AddSearchAsync(PubMedSearch search)
        {
            return Task.Run(() =>  AddSearch(search) );
        }

        public void AddSearch(PubMedSearch search)
        {
            OperationOnDatabase.WaitOne();

            var itemToDelete = (from s in _db.PubMedSearch
                                where String.Equals(s.SearchString, search.SearchString) == true
                                select s).FirstOrDefault();
            bool isBookmarked = false;
            if (itemToDelete != null)
            {
                isBookmarked = itemToDelete.IsBookmarked;
                Dispose(itemToDelete);
            }

            DbSearch itemToAdd = new DbSearch
            {
                SearchString = search.SearchString,
                DateRequested = search.DateRequested,
                DateItemsUpdated = search.DateRequested,
                Count = search.Count,
                NewItems = 0,
                WebEnviroment = search.WebEnviroment,
                IsBookmarked = isBookmarked,
                Query = search.Query,
                SearchName = search.SearchName
            };
            
            _db.PubMedSearch.InsertOnSubmit(itemToAdd);
            _db.SubmitChanges();
            
            search.Id = itemToAdd.Id;
            List<DbSearchItem> searchItems = AddSearchItems(search);
               
            _db.SearchItems.InsertAllOnSubmit(searchItems);
            _db.SubmitChanges();

            OperationOnDatabase.Set();
            
        }

        private List<DbSearchItem> AddSearchItems(PubMedSearch search)
        {
            List<DbSearchItem> results = new List<DbSearchItem>();
            for (int i = 0; i < search.PIdList.Count; i++)
            {
                DbSearchItem item = new DbSearchItem
                {
                    Order = i,
                    PMId = search.PIdList[i],
                    SeachId = search.Id
                };
                results.Add(item);
            }
            return results;

        }

        public Task<ObservableCollection<PubMedSearch>> GetSearchListAsync()
        {
            return Task<ObservableCollection<PubMedSearch>>.Run(() => { return GetSearchList(); });
        }
        
        public ObservableCollection<PubMedSearch> GetSearchList()
        {
            OperationOnDatabase.WaitOne();

            ObservableCollection<PubMedSearch> results = new ObservableCollection<PubMedSearch>();
            var q = (from s in _db.PubMedSearch
                    orderby s.DateRequested descending
                    select s).Take(RecentSearchListMaxContent).ToList();
            foreach (DbSearch item in q)
            {
                PubMedSearch result = MapSearch(item); 
                results.Add(result);
            }

            OperationOnDatabase.Set();
            return results;
            
        }

        public Task<PubMedSearch> GetSearchAsync(int searchId)
        {
            return Task<PubMedSearch>.Run(() => { return GetSearch(searchId); });
        }

        public PubMedSearch GetSearch(int searchId)
        {
            OperationOnDatabase.WaitOne();
            
            DbSearch q = (from s in _db.PubMedSearch
                    where s.Id ==  searchId
                    select s).SingleOrDefault();

            PubMedSearch result = MapSearch(q);

            OperationOnDatabase.Set();
            return result;
        }

        
        #endregion

        #region Spotlight

        public Task<ObservableCollection<PubMedSearch>> GetSpotLightListAsync()
        {
            return Task<ObservableCollection<PubMedSearch>>.Run(() => { return GetSpotLightList(); });
        }

        public ObservableCollection<PubMedSearch> GetSpotLightList()
        {
            OperationOnDatabase.WaitOne();

            ObservableCollection<PubMedSearch> results = new ObservableCollection<PubMedSearch>();
            var q = from s in _db.PubMedSearch
                    orderby s.DateRequested
                    where s.IsBookmarked
                    select s;
            foreach (DbSearch item in q)
            {
                PubMedSearch result = MapSearch(item);
                results.Add(result);
            }

            OperationOnDatabase.Set();
            return results;
        }

        public Task SetBookmarkSearchAsync(int id, bool isBookmarked)
        {
            return Task.Run( () => SetBookmarkSearch(id, isBookmarked));
        }

        public void SetBookmarkSearch(int id, bool isBookmarked)
        {
            OperationOnDatabase.WaitOne();

            DbSearch item = (from s in _db.PubMedSearch
                             where s.Id == id
                             select s).SingleOrDefault();
            if (item != null)
            {
                item.IsBookmarked = isBookmarked;
                lock (_db)
                {
                    _db.SubmitChanges();
                }
            }

            OperationOnDatabase.Set();
        }

        public Task UpdateSearchCountAsync(int searchId, int count)
        {
            return Task.Run(() => UpdateSearchCount(searchId, count));
        }

        public void UpdateSearchCount(int searchId, int count)
        {
            OperationOnDatabase.WaitOne();
            DbSearch q = (from s in _db.PubMedSearch
                          where s.Id == searchId
                          select s).SingleOrDefault();
            if (q != null)
            {
                q.NewItems = count;
                q.DateItemsUpdated = DateTime.UtcNow;
                _db.SubmitChanges();
            }
            OperationOnDatabase.Set();
        }

        public Task<PubMedSearch> GetNextSearchForCountUpdateAsync()
        {
            return Task<PubMedSearch>.Run(() => { return GetNextSearchForCountUpdate(); });
        }

        public PubMedSearch GetNextSearchForCountUpdate()
        {
            OperationOnDatabase.WaitOne();
            DbSearch q = (from s in _db.PubMedSearch
                          where s.IsBookmarked
                          orderby s.DateItemsUpdated ascending
                          select s).Take(1).SingleOrDefault();

            if (q != null)
            {
                PubMedSearch result = MapSearch(q);
                OperationOnDatabase.Set();
                return result;
            }
            OperationOnDatabase.Set();
            return null;
        }

        public Task<int> GetTotalNewResultInSearchAsync()
        {
            return Task<int>.Run(() => { return GetTotalNewResultInSearch(); });
        }
        
        public int GetTotalNewResultInSearch()
        {
            OperationOnDatabase.WaitOne();
            List<int> list = (from s in _db.PubMedSearch
                          where s.IsBookmarked
                          select s.NewItems).ToList();
            OperationOnDatabase.Set();
            int result = 0;
            if (list.Count != 0)
            {
                result = list.Sum();
            }
            
            return result;
        }

        public Task UpdateSearchNameAsync(PubMedSearch spotlightItem)
        {
            return Task.Run(() => UpdateSearchName(spotlightItem));
        }

        public void UpdateSearchName(PubMedSearch spotlightItem)
        {
            OperationOnDatabase.WaitOne();
            DbSearch q = (from s in _db.PubMedSearch where s.Id == spotlightItem.Id select s).SingleOrDefault();
            if (q != null)
            {
                q.SearchName = spotlightItem.SearchName;
                _db.SubmitChanges();
            }
            OperationOnDatabase.Set();
        }

        #endregion

        #region Summaries

        public Task<List<PubMedSummary>> GetAvailableSummariesAsync(List<int> searchList)
        {
            return Task<List<PubMedSummary>>.Run(() => { return GetAvailableSummaries(searchList); });
        }
        
        public List<PubMedSummary> GetAvailableSummaries(List<int> searchList)
        {
            OperationOnDatabase.WaitOne();
            List<DbPubmedArticle> q = (from s in _db.PubMedArticle
                                 where searchList.Contains(s.PMId)
                                 select s).ToList();
            List<PubMedSummary> results = new List<PubMedSummary>();
            results.BuildIndexedList(searchList);
            foreach (DbPubmedArticle item in q)
            {
                if (item != null)
                {
                    PubMedSummary result = new PubMedSummary();
                    result.MapFromDataBase(item);
                    item.TimeStamp = DateTime.UtcNow;
                    result.AuthorList = GetAuthorList(item.PMId);
                    results.AddToIndexedList(result);
                }
                
            }
            _db.SubmitChanges();
            
            OperationOnDatabase.Set();
            return results;
        }

        public Task<PubMedSummary> GetSummaryAsync(int i)
        {
            return Task<PubMedSummary>.Run(() => { return GetSummary(i); });
        }
        
        public PubMedSummary GetSummary(int i)
        {
            OperationOnDatabase.WaitOne();
            PubMedSummary result = GetSingleSummary(i);
            OperationOnDatabase.Set();
            return result; 
        }



        private PubMedSummary GetSingleSummary(int i)
        {
            DbPubmedArticle q =  ( from s in _db.PubMedArticle
                            where s.PMId == i
                            select s).SingleOrDefault();
            if (q != null)
            {
                PubMedSummary result =  new PubMedSummary();
                result.MapFromDataBase(q);
                q.TimeStamp = DateTime.UtcNow;
                _db.SubmitChanges();
                result.AuthorList = GetAuthorList(i);
                return result;
            }
            else
            {
                return null;
            }
        }

        private List<string> GetAuthorList(int id)
        {
            List<string> q = (from a in _db.PaperAuthors
                              where a.PMId == id
                              orderby a.Order
                              select a.Author).ToList();
            return q;
        }

        public Task AddSummaryAsync(IEnumerable<PubMedSummary> summary)
        {
            return Task.Run(() => AddSummary(summary));
        }

        public void AddSummary(IEnumerable<PubMedSummary> summary)
        {
            OperationOnDatabase.WaitOne();

            List<DbPubmedArticle> articlesToAdd = new List<DbPubmedArticle>();
            List<DbPaperAuthors> authorListToAdd = new List<DbPaperAuthors>();
            foreach (PubMedSummary item in summary)
            {
                if (item != null)
                {
                    if (!articlesToAdd.Any(m => m.PMId == item.PMId) && item.PMId != 0)
                    {
                        DbPubmedArticle dbItem = new DbPubmedArticle
                        {
                            PMId = item.PMId,
                            Title = item.Title,
                            AuthorSummary = item.AuthorSummary,
                            PubDate = item.PubDate,
                            Source = item.Source,
                            SourceRef = item.SourceRef,
                            AbstractAquired = item.IsAbstractAcquired,
                            IsOnReadingList = item.IsOnReadingList,
                            TimeStamp = DateTime.UtcNow
                        };
                        articlesToAdd.Add(dbItem);
                        List<DbPaperAuthors> authorList = AddPaperAuthors(item);
                        authorListToAdd.AddRange(authorList);
                    }
                    else
                    {

                    }
                }
            }
            lock (_db)
            {
                _db.PubMedArticle.InsertAllOnSubmit(articlesToAdd);
                _db.PaperAuthors.InsertAllOnSubmit(authorListToAdd);
                _db.SubmitChanges();
            }

            OperationOnDatabase.Set();
        }

        private List<DbPaperAuthors> AddPaperAuthors(PubMedSummary summary)
        {
            List<DbPaperAuthors> results = new List<DbPaperAuthors>();
            for (int i = 0; i < summary.AuthorList.Count; i++)
            {
                DbPaperAuthors item = new DbPaperAuthors
                {
                    Order = i,
                    PMId = summary.PMId,
                    Author = summary.AuthorList[i]
                };
                results.Add(item);
            }
            return results;
        }

        #endregion
        
        #region Abstracts
        public PubMedAbstract GetAbstract(int id)
        {
            OperationOnDatabase.WaitOne();
            
            DbPubmedArticle q = (from s in _db.PubMedArticle
                                 where s.PMId == id
                                 select s).SingleOrDefault();
            if (q != null)
            {
                PubMedAbstract result = new PubMedAbstract();
                result.Summary = new PubMedSummary
                {
                    PMId = q.PMId,
                    PubDate = q.PubDate,
                    AuthorSummary = q.AuthorSummary,
                    Source = q.Source,
                    Title = q.Title,
                    IsAbstractAcquired = q.AbstractAquired,
                    SourceRef = q.SourceRef,
                    IsOnReadingList = q.IsOnReadingList
                    
                };
                result.Summary.AuthorList = GetAuthorList(id);
                result.Abstract = q.Abstract;
                result.PMCId = q.PMCId;

                OperationOnDatabase.Set();
                return result;
            }
            else
            {
                OperationOnDatabase.Set();
                return null;
            }
        }

        public Task AddAbstractAsync(PubMedSummary result)
        {
            return Task.Run(() => AddAbstract(result));
        }

        public void AddAbstract(PubMedSummary result)
        {
            if (result != null)
            {
                OperationOnDatabase.WaitOne();

                DbPubmedArticle q = (from s in _db.PubMedArticle
                                     where s.PMId == result.PMId
                                     select s).SingleOrDefault();
                if (q != null)
                {
                    q.Abstract = result.Abstract;
                    q.AbstractAquired = result.IsAbstractAcquired;
                    q.PMCId = result.PMCId;
                }
                else
                {
                    q = new DbPubmedArticle()
                    {
                        Abstract = result.Abstract,
                        AbstractAquired = result.IsAbstractAcquired,
                        PMId = result.PMId,
                        Title = result.Title,
                        AuthorSummary = result.AuthorSummary,
                        PubDate = result.PubDate,
                        Source = result.Source,
                        SourceRef = result.SourceRef,
                        IsOnReadingList = result.IsOnReadingList,
                        PMCId = result.PMCId
                    };
                    List<DbPaperAuthors> authorList = AddPaperAuthors(result);
                    _db.PubMedArticle.InsertOnSubmit(q);
                    _db.PaperAuthors.InsertAllOnSubmit(authorList);
                }
                lock (_db)
                {
                    _db.SubmitChanges();
                }
                OperationOnDatabase.Set();
            }
        }
        #endregion

        #region Notes
        public List<DbAbstractNote> GetNotes(int id)
        {
            OperationOnDatabase.WaitOne();
            List<DbAbstractNote> result = (from n in _db.AbstractNotes where n.PMId == id select n).ToList();
            OperationOnDatabase.Set();
            return result;
        }

        public Task AddNoteAsync(DbAbstractNote note)
        {
            return Task.Run(() => AddNote(note));
        }

        public void AddNote(DbAbstractNote note)
        {
            OperationOnDatabase.WaitOne();
            lock (_db)
            {
                _db.AbstractNotes.InsertOnSubmit(note);
                _db.SubmitChanges();
            }
            OperationOnDatabase.Set();
        }

        public Task RemoveNoteAsync(DbAbstractNote note)
        {
            return Task.Run(() => RemoveNote(note));
        }

        public void RemoveNote(DbAbstractNote note)
        {
            OperationOnDatabase.WaitOne();
            lock (_db)
            {
                _db.AbstractNotes.DeleteOnSubmit(note);
                _db.SubmitChanges();
            }
            OperationOnDatabase.Set();
        }
        #endregion

        #region Reading List

        public ObservableCollection<PubMedSummary> GetReadingList()
        {
            List<int> q = (from s in _db.PubMedArticle where s.IsOnReadingList select s.PMId).ToList();

            ObservableCollection<PubMedSummary> results = new ObservableCollection<PubMedSummary>();
            foreach (int i in q)
            {
                results.Add(GetSummary(i));
            }
            return results;
        }

        public Task SetReadingListAsync(int pMId, bool isOnReadinList)
        {
            return Task.Run(() => SetReadingList(pMId, isOnReadinList));
        }

        public void SetReadingList(int pMId, bool isOnReadinList)
        {
            OperationOnDatabase.WaitOne();
            
            var q = (from a in _db.PubMedArticle where a.PMId == pMId select a).SingleOrDefault();
            if (q != null)
            {
                q.IsOnReadingList = isOnReadinList;
                _db.SubmitChanges();
            }
            OperationOnDatabase.Set();
        }

        #endregion

        #region Disposal Methods

        public async Task CleanDb()
        {
            await CleanSearchListAsync();
            await CleanArticleListAsync();
        }

        public Task CleanSearchListAsync()
        {
            return Task.Run(() => CleanSearchList());
        }

        private void CleanSearchList()
        {
            OperationOnDatabase.WaitOne();

            List<DbSearch> searchList = (from s in _db.PubMedSearch
                                         orderby s.DateRequested descending
                                         select s).Skip(RecentSearchListMaxContent).ToList();
            List<DbSearch> itemsToDelete = new List<DbSearch>();
            List<DbSearchItem> searchItemToDelete = new List<DbSearchItem>();
            foreach (DbSearch item in searchList)
            {
                if (!item.IsBookmarked)
                {
                    itemsToDelete.Add(item);
                    List<DbSearchItem> searchitems = (from si in _db.SearchItems where si.SeachId == item.Id select si).ToList();
                    searchItemToDelete.AddRange(searchitems);
                }
            }

            if (itemsToDelete.Count > 0)
            {
                lock (_db)
                {
                    _db.SearchItems.DeleteAllOnSubmit(searchItemToDelete);
                    _db.PubMedSearch.DeleteAllOnSubmit(itemsToDelete);
                    _db.SubmitChanges();
                }
            }

            OperationOnDatabase.Set();
        }

        public Task CleanArticleListAsync()
        {
            return Task.Run(() => CleanArticleList());
        }

        public void CleanArticleList()
        {
            OperationOnDatabase.WaitOne();
            List<DbPubmedArticle> articleTodelete = (from s in _db.PubMedArticle
                                                     where !s.IsOnReadingList
                                                     orderby s.TimeStamp descending
                                                     select s).Skip(MaximunNumberOfArticles).ToList();
            if (articleTodelete.Count > 0)
            {
                List<DbPaperAuthors> authorsToDelete = new List<DbPaperAuthors>();
                List<DbAbstractNote> notesToDelete = new List<DbAbstractNote>();
                foreach (DbPubmedArticle item in articleTodelete)
                {
                    List<DbPaperAuthors> itemAuthors = (from a in _db.PaperAuthors
                                                        where a.PMId == item.PMId
                                                        select a).ToList();
                    List<DbAbstractNote> itemNotes = (from n in _db.AbstractNotes
                                                      where n.PMId == item.PMId
                                                      select n).ToList();
                    authorsToDelete.AddRange(itemAuthors);
                    notesToDelete.AddRange(itemNotes);
                }
                lock (_db)
                {
                    _db.PaperAuthors.DeleteAllOnSubmit(authorsToDelete);
                    _db.AbstractNotes.DeleteAllOnSubmit(notesToDelete);
                    _db.PubMedArticle.DeleteAllOnSubmit(articleTodelete);
                    _db.SubmitChanges();
                }
            }

            OperationOnDatabase.Set();
        }

        

        private void Dispose(DbSearch item)
        {
            List<DbSearchItem> searchitems = (from si in _db.SearchItems where si.SeachId == item.Id select si).ToList();
            lock (_db)
            {
                _db.PubMedSearch.DeleteOnSubmit(item);
                _db.SearchItems.DeleteAllOnSubmit(searchitems);
                _db.SubmitChanges();
            }
        }

        #endregion

        private PubMedSearch MapSearch(DbSearch q)
        {
            if (q != null)
            {
                PubMedSearch result = new PubMedSearch
                {
                    Id = q.Id,
                    DateRequested = q.DateRequested,
                    Count = q.Count,
                    NewItems = q.NewItems,
                    SearchString = q.SearchString,
                    WebEnviroment = q.WebEnviroment,
                    IsBookMarked = q.IsBookmarked,
                    Query = q.Query,
                    SearchName = q.SearchName
                };
                var q2 = from si in _db.SearchItems
                         where si.SeachId == q.Id
                         orderby si.Order
                         select si.PMId;
                result.PIdList = q2.ToList();
                return result;
            }
            else
            {
                return null;
            }
        }

    }
}
