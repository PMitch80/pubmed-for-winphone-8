﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    [Table]
    public class DbPubmedArticle
    {
        [Column(IsPrimaryKey= true)]
        public int PMId {get; set;}
        [Column]
        public string PMCId { get; set; }
        [Column]
        public string AuthorSummary { get; set; }
        [Column]
        public DateTime? PubDate {get; set;}
        [Column]
        public string Source {get; set;}
        [Column]
        public string SourceRef { get; set; }
        [Column]
        public string Title {get; set;}
        [Column]
        public bool AbstractAquired { get; set; }
        [Column]
        public bool IsOnReadingList { get; set; }
        [Column]
        public string Abstract { get; set; }
        [Column]
        public DateTime TimeStamp { get; set; }
    }
}
