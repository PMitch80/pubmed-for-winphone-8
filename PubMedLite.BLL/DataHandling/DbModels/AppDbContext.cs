﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    public partial class AppDbContext : DataContext
    {
        public Table<DbPubmedArticle> PubMedArticle;
        public Table<DbPaperAuthors> PaperAuthors;
        public Table<DbSearch> PubMedSearch;
        public Table<DbSearchItem> SearchItems;
        public Table<DbAbstractNote> AbstractNotes;

        public AppDbContext(string connection) : base(connection) { }

    }
}
