﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    [Table]
    public class DbSearchItem
    {
        [Column(IsPrimaryKey=true, IsDbGenerated=true)]
        public int Id {get; set;}
        [Column]
        public int SeachId {get; set;}
        [Column]
        public int PMId {get; set;}
        [Column]
        public int Order {get; set;}

    }
}
