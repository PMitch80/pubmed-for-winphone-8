﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    [Table]
    public class DbAbstractNote
    {
        [Column(IsPrimaryKey = true, IsDbGenerated=true)]
        public int Id { get; set; }
        [Column]
        public int PMId { get; set; }
        [Column]
        public string Note { get; set; }
    }
}
