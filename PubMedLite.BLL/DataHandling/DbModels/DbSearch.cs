﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubMedLite.BLL
{
    [Table]
    public class DbSearch
    {
        [Column(IsPrimaryKey=true, IsDbGenerated=true)]
        public int Id { get; set; }
        [Column]
        public string SearchName { get; set; }
        [Column]
        public string SearchString { get; set; }
        [Column]
        public int Query { get; set; }
        [Column]
        public DateTime DateRequested { get; set; }
        [Column]
        public int Count { get; set; }
        [Column]
        public string WebEnviroment { get; set; }
        [Column]
        public bool IsBookmarked { get; set; }
        [Column]
        public DateTime DateItemsUpdated { get; set; }
        [Column]
        public int NewItems { get; set; }
    }
}
