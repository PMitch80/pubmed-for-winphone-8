﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace PubMedLite.BLL
{
    public static class UIThread
    {
        private static readonly Dispatcher _dispatcher;

        static UIThread()
        {
            _dispatcher = Deployment.Current.Dispatcher;
        }

        public static void Invoke(Action action)
        {
            if (_dispatcher == null)
            {
                action.Invoke();
            }
            else
            {
                _dispatcher.BeginInvoke(action);
            }
        }
    }
}
