﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using PubMedLite.BLL;
using Microsoft.Phone.Shell;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace PubMedLite.TileUpdater
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });

            
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected async override void OnInvoke(ScheduledTask task)
        {
            AppDbContext AppDb = new AppDbContext("isostore:/AppDb.sdf");
            if (AppDb.DatabaseExists())
            {
                AppDbRepository repository = new AppDbRepository(AppDb);
                PubMedSearchService _service = new PubMedSearchService(repository);
                PubMedSearch search =  await _service.UpdateNextSearch();
                if (search != null)
                {
                    Uri secondaryTileUri = new Uri("/Pages/AdvanceSearchPage.xaml?id=" + search.Id, UriKind.Relative);
                    ShellTile secondaryTile = ShellTile.ActiveTiles.FirstOrDefault(t => t.NavigationUri == secondaryTileUri);
                    Task<int> countTask = _service.GetNewResultTotal();
                    if (secondaryTile != null)
                    {
                        IconicTileData secondaryData = new IconicTileData();
                        secondaryData.Count = search.NewItems;
                        secondaryTile.Update(secondaryData);
                    }
                    int i = await countTask;
                    ShellTile TitleTile = ShellTile.ActiveTiles.FirstOrDefault();
                    IconicTileData data = new IconicTileData();
                    data.Count = i;
                    TitleTile.Update(data);
                    
                }
            }
            NotifyComplete();
        }
    }
}